import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})

export class ToastService {
    toaster: any;
    constructor(
        private toastCtrl: ToastController
    ) { }

    show(options?: IToastOption) {
        return new Promise(async (resolve?: any, reject?: any) => {

            if (options && options.message) {
                this.toaster = await this.toastCtrl.create({
                    message: options.message,
                    duration: options.duration ? options.duration : 5000,
                    position: 'bottom',
                    cssClass: 'my-toast-wrapper',

                });

                this.toaster.onDidDismiss((data, role) => {
                    if (role == 'messageClick') {
                        resolve(data);
                    } else {
                        reject();
                    }
                });

                this.toaster.present();
            }
            else {
                console.error('Pass object in toast');
            }
        });
    }

}
