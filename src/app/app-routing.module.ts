import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'tabs',
    loadChildren: () => import('./pages/tabs/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'phone-auth',
    loadChildren: () => import('./pages/phone-auth/phone-auth.module').then(m => m.PhoneAuthPageModule)
  },
  {
    path: 'phone-verification',
    loadChildren: () => import('./pages/phone-verification/phone-verification.module').then(m => m.PhoneVerificationPageModule)
  },
  {
    path: 'select-country',
    loadChildren: () => import('./modals/select-country/select-country.module').then(m => m.SelectCountryPageModule)
  },
  {
    path: 'edit-profile',
    loadChildren: () => import('./pages/edit-profile/edit-profile.module').then( m => m.EditProfilePageModule)
  },
  {
    path: 'create-order',
    loadChildren: () => import('./modals/create-order/create-order.module').then( m => m.CreateOrderPageModule)
  },
  {
    path: 'select-location',
    loadChildren: () => import('./pages/select-location/select-location.module').then( m => m.SelectLocationPageModule)
  },
  {
    path: 'welcome',
    loadChildren: () => import('./pages/welcome/welcome.module').then( m => m.WelcomePageModule)
  },
  {
    path: 'product-detail',
    loadChildren: () => import('./pages/product-detail/product-detail.module').then( m => m.ProductDetailPageModule)
  },
  {
    path: 'credit-cards',
    loadChildren: () => import('./pages/credit-cards/credit-cards.module').then( m => m.CreditCardsPageModule)
  },
  {
    path: 'add-card',
    loadChildren: () => import('./pages/add-card/add-card.module').then( m => m.AddCardPageModule)
  },
  {
    path: 'block-user',
    loadChildren: () => import('./pages/block-user/block-user.module').then( m => m.BlockUserPageModule)
  },
  {
    path: 'driver-tabs',
    loadChildren: () => import('./driver/pages/tabs/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'product-order',
    loadChildren: () => import('./pages/product-order/product-order.module').then( m => m.ProductOrderPageModule)
  },
  {
    path: 'shopping-cart',
    loadChildren: () => import('./pages/shopping-cart/shopping-cart.module').then( m => m.ShoppingCartPageModule)
  },
  {
    path: 'product-order-detail',
    loadChildren: () => import('./pages/product-order-detail/product-order-detail.module').then( m => m.ProductOrderDetailPageModule)
  },
  {
    path: 'raise-a-complaint',
    loadChildren: () => import('./pages/raise-a-complaint/raise-a-complaint.module').then( m => m.RaiseAComplaintPageModule)
  },
  {
    path: 'change-language',
    loadChildren: () => import('./pages/change-language/change-language.module').then( m => m.ChangeLanguagePageModule)
  },
  {
    path: 'total-orders',
    loadChildren: () => import('./pages/total-orders/total-orders.module').then( m => m.TotalOrdersPageModule)
  },
  {
    path: 'driver-main',
    loadChildren: () => import('./driver/pages/tabs/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'orders',
    loadChildren: () => import('./driver/modals/orders/orders.module').then( m => m.OrdersPageModule)
  },
  {
    path: 'history',
    loadChildren: () => import('./driver/pages/history/history.module').then( m => m.HistoryPageModule)
  },
  {
    path: 'user-detail-during-order',
    loadChildren: () => import('./modals/user-detail-during-order/user-detail-during-order.module').then( m => m.UserDetailDuringOrderPageModule)
  },
  {
    path: 'chat-view',
    loadChildren: () => import('./modals/chat-view/chat-view.module').then( m => m.ChatViewPageModule)
  },
  {
    path: 'raise-a-complaint',
    loadChildren: () => import('./modals/raise-a-complaint/raise-a-complaint.module').then( m => m.RaiseAComplaintPageModule)
  },
  {
    path: 'track-driver',
    loadChildren: () => import('./pages/track-driver/track-driver.module').then( m => m.TrackDriverPageModule)
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
