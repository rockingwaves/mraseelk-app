import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { SupplantPipe } from './supplant.pipe';
import { FromNow } from './fromNow';
import { CustomeMoment } from './customeMoment';
import { AddressMakerPipe } from './address-maker.pipe';
import { SubstringPipe } from './substring.pipe';

@NgModule({
    imports: [IonicModule, CommonModule],
    declarations: [SupplantPipe, FromNow, CustomeMoment, AddressMakerPipe, SubstringPipe],
    exports: [SupplantPipe, FromNow, CustomeMoment, AddressMakerPipe, SubstringPipe]
})
export class PipesModule { }