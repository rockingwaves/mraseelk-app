import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductOrderPageRoutingModule } from './product-order-routing.module';

import { ProductOrderPage } from './product-order.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductOrderPageRoutingModule,
    TranslateModule
  ],
  declarations: [ProductOrderPage]
})
export class ProductOrderPageModule { }
