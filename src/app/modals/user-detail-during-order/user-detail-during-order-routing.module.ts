import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserDetailDuringOrderPage } from './user-detail-during-order.page';

const routes: Routes = [
  {
    path: '',
    component: UserDetailDuringOrderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserDetailDuringOrderPageRoutingModule {}
