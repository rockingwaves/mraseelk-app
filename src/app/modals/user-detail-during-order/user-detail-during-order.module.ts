import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserDetailDuringOrderPageRoutingModule } from './user-detail-during-order-routing.module';

import { UserDetailDuringOrderPage } from './user-detail-during-order.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserDetailDuringOrderPageRoutingModule
  ],
  declarations: []
})
export class UserDetailDuringOrderPageModule {}
