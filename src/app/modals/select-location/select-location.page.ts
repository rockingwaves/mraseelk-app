import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LocationService } from 'src/app/services/location.service';
import { ModalController, IonSlides, NavParams } from '@ionic/angular';

declare var google;
@Component({
  selector: 'app-select-location',
  templateUrl: './select-location.page.html',
  styleUrls: ['./select-location.page.scss'],
})
export class SelectLocationPage implements OnInit {
  @ViewChild(IonSlides, { static: true }) slides: IonSlides;
  @ViewChild("map", { static: true }) elMap: ElementRef;
  private map;

  private location: ILatLng = null;

  step: number = 0;
  private latLng: ILatLng = this.navParams.get('latLng');
  address: IAddress = {
    address1: '',
    address2: '',
    city: '',
    country: '',
    state: '',
    zipCode: ''
    
  }
  constructor(
    private srvLocation: LocationService,
    private modal: ModalController,
    private navParams: NavParams
  ) { }

  ngOnInit() {
    console.log("latLng", this.latLng);
    this.slides.lockSwipes(true);
    if (this.latLng) { this.location = this.latLng; this.loadMap(); return; }
    this.srvLocation.getCurrentLocation().then((location) => {
      this.location = location.coords;
      this.loadMap();
    });

  }

  initSearchbar() {
    setTimeout(() => {
      this.map = new google.maps.Map(
        this.elMap.nativeElement,
        {
          center: { lat: parseFloat(this.location.latitude), lng: parseFloat(this.location.longitude) },
          zoom: 16,
          disableDefaultUI: true
        });
      var input = document.getElementById('pac-input');
      var options: any = {};
      // if (this.user.selectedCountry && this.user.selectedCountry.i) {
      //   options.componentRestrictions = { country: this.user.selectedCountry.i }
      // }
      var searchBox = new google.maps.places.Autocomplete(input, options);
      google.maps.event.addListener(searchBox, 'place_changed', () => {
        var place = searchBox.getPlace();
        if (place) {
          if (place.geometry) {
            place = place.geometry;
            place = place.location;
            if (place) {
              this.map.panTo(new google.maps.LatLng(place.lat(), place.lng()));
            }
          }
        }
      });
    }, 400)
  }

  loadMap() {
    let options = {
      center: { lat: this.location.latitude, lng: this.location.longitude },
      disableDefaultUI: true,
      mapTypeControl: false,
      draggable: true,
      clickableIcons: false,
      zoom: 16,

    };

    this.map = new google.maps.Map(this.elMap.nativeElement, options);
    this.initSearchbar();
  }

  selectLocation(callback: Function) {
    let coords: ILatLng = {
      latitude: this.map.getCenter().lat(),
      longitude: this.map.getCenter().lng()
    };
    this.location = coords;
    callback && callback();
    // this.modal.dismiss(coords);
  }

  goBack() {
    if (this.step == 0) {
      this.modal.dismiss();
    } else {
      this.goToSlide1();
    }
  }

  conitnue() {
    console.log(this.step);
    if (this.step == 0) {
      this.selectLocation(() => {
        this.srvLocation.geocode(this.location).then((res) => {
          console.log(res);
          this.address = res.data;
          // this.goToSlide2();
          this.dismiss();
        })
      })
    }
  }

  goToSlide1() {
    this.slides.lockSwipes(false);
    this.slides.slideTo(0);
    this.slides.lockSwipes(true);
  }


  goToSlide2() {
    this.slides.lockSwipes(false);
    this.slides.slideTo(1);
    this.slides.lockSwipes(true);
  }

  slideChanged() {
    this.slides.getActiveIndex().then((index) => {
      console.log(index);
      this.step = index;
      console.log(this.step);
    });

  }


  dismiss() {
    this.modal.dismiss({ address: this.address, latlng: this.location });
  }


}
