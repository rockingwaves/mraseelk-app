enum ESelectType {
    SCHOOL = 'school',
    UNIVERSITY = 'university'
}

export enum EChangeInfo {
    PRIVATE_INFO = 'PRIVATE_INFO',
    PUBLIC_INFO = 'PUBLIC_INFO'
}