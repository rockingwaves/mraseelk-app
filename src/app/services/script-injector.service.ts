import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class ScriptInjectorService {
  private scripts = {};
  private ScriptStore: any[] = [
    // AIzaSyBnQ2J0Rry2pBlQUibQ-RqGflRA9DeF6TI
    { name: 'google', src: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBBl0sKsT4HP-YGy7WIeX6_5Km8wUHYTns&libraries=places,geometry', type: "script" },
    { name: 'richmarker', src: 'assets/js/richmarker.js', type: "script" }
    // { name: 'jquery-min', src: 'assets/js/jquery-1.10.2.min.js', type: "script" },
    // { name: 'barcode', src: 'assets/js/barcode.min.js', type: "script" },
    // { name: 'phone-formate', src: 'assets/js/phone-formate.js', type: "script" },
    // { name: "jquery-mask", src: 'assets/js/jquery.mask.min.js', type: "script" },
    // { name: "aws-sdk", src: 'assets/js/aws-sdk-2.16.0.min.js', type: "script" },
    // { name: "croppiejs", src: 'assets/js/croppie.js', type: "script" },
    // { name: "croppiecss", src: 'assets/js/croppie.css', type: "stylesheet" },
    // { name: 'qrcode', src: 'assets/js/qrcode.js', type: "script" }
    // { name: "logger", src: 'assets/js/logger.js' }
  ]

  constructor() {
    this.ScriptStore.forEach((script) => {
      this.scripts[script.name] = {
        loaded: false,
        src: script.src,
        type: script.type
      };
    });
  }

  load(...scripts: string[]) {
    var promises: Promise<{ script: string; loaded: boolean; status: string; }>[] = [];
    scripts.forEach((script) => promises.push(this.loadScript(script)));
    return Promise.all(promises);
  }

  loadScript(name: string): Promise<{ script: string; loaded: boolean; status: string; }> {
    return new Promise((resolve, reject) => {
      //resolve if already loaded
      if (this.scripts[name].loaded) {
        resolve({ script: name, loaded: true, status: 'Already Loaded' });
      }
      else {
        //load script
        let script = this.scripts[name];
        let elemType = script.type == "script" ? "script" : "link";
        let el = <any>document.createElement(elemType);

        if (elemType == "script") {
          let elem: HTMLScriptElement = el;
          elem.type = 'text/javascript';
          elem.src = this.scripts[name].src;
          // elem
          // if (elem.readyState) {  //IE
          //     elem.onreadystatechange = () => {
          //         if (elem.readyState === "loaded" || elem.readyState === "complete") {
          //             elem.onreadystatechange = null;
          //             this.scripts[name].loaded = true;
          //             resolve({ script: name, loaded: true, status: 'Loaded' });
          //         }
          //     };
          // } else {  //Others
          elem.onload = () => {
            this.scripts[name].loaded = true;
            console.log(name + ": Loaded");
            resolve({ script: name, loaded: true, status: 'loaded' });
          };
          // }
          elem.onerror = (error: any) =>
            console.log("error1", error); resolve({ script: name, loaded: false, status: 'not_loaded' });
          document.getElementsByTagName('head')[0].appendChild(elem);
        }
        else if (elemType == "link") {
          let elem: HTMLLinkElement = el;
          elem.rel = "stylesheet";
          elem.href = script.src;
          elem.onload = () => {
            this.scripts[name].loaded = true;
            console.log(name + ": Loaded");
            resolve({ script: name, loaded: true, status: 'loaded' });
          };
          elem.onerror = (error: any) => console.log("error", error); resolve({ script: name, loaded: false, status: 'not_loaded' });
          document.getElementsByTagName('head')[0].appendChild(elem);
        }
      }
    });
  }
}

export interface IScript {
  name: string;
  isLoaded: boolean;
  url: string;
}