import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { SharedServiceService } from 'src/app/services/shared-service.service';
import { NavController } from '@ionic/angular';
import { FirebaseService } from 'src/app/services/firebase.service';
import { LoadingService } from 'src/app/services/loading.service';
import { NavigationExtras } from '@angular/router';
import * as _ from 'lodash';
import { LocationService } from 'src/app/services/location.service';
import * as geolib from 'geolib';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  user: IUser = this.srvUser.get();
  pendingOrders: IOrder[] = [];
  myLoc: ILatLng = { latitude: null, longitude: null };
  constructor(
    private srvUser: UserService,
    // private firebaseService: FirebaseService,
    private loadingService: LoadingService,
    private navCtrl: NavController,
    private cdh: ChangeDetectorRef,
    private srvLocation: LocationService,
    private srvFirebase: FirebaseService,

  ) {
    this.getOrders();

  }

  ngOnInit() {
    console.log("geolib", geolib);
    this.srvLocation.getCurrentLocation().then((loc) => {
      this.myLoc = {
        latitude: loc.coords.latitude,
        longitude: loc.coords.longitude
      }
      this.getOrders();
    });
  }


  getOrders() {
    this.srvFirebase.getPendingOrders((resp: ILocalResp) => {
      console.log("resp.status", resp.status);
      switch (resp.status) {
        case "OK":
          this.pendingOrders = [];
          let orderss = [];
          orderss = _.filter(resp.message, (order: IShoppingCart) => { return (order.status != 'completed') }).reverse();
          _.forEach(orderss, (o: IOrder) => {
            let distance = geolib.getDistance(o.pickupLoc.latlng, this.myLoc) / 1000;
            let index = _.findIndex(this.pendingOrders, (o_) => { return o_.orderId == o.orderId });
            if (distance <= 20 && index == -1) {
              this.pendingOrders.push(o);
            }
            console.log("this.pendingOrders", this.pendingOrders.length);
          });



          break;

        default:
          break;
      }
    });

    this.updateDriverLoc();

  }

  goToDetails(order) {
    let params: NavigationExtras = {
      queryParams: {
        order: order
      }
    }
    this.navCtrl.navigateForward('driver-main', params);
  }


  updateDriverLoc() {
    console.log("i am getting location");
    this.srvLocation.getCurrentLocation({ enableHighAccuracy: true }).then((loc) => {
      console.log(this.user);
      this.srvFirebase.updatDriverInfo(this.user, {
        latitude: loc.coords.latitude,
        longitude: loc.coords.longitude
      });
    }).catch((err) => { console.log(err) });
  }


}
