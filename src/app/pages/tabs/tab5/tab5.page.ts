import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { NavController } from '@ionic/angular';
import { FirebaseService } from 'src/app/services/firebase.service';
import { StorageService } from 'src/app/services/storage.service';
import { LoadingService } from 'src/app/services/loading.service';
import { AlertService } from 'src/app/services/alert.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tab5',
  templateUrl: './tab5.page.html',
  styleUrls: ['./tab5.page.scss'],
})
export class Tab5Page implements OnInit {
  user: IUser = this.srvUser.get();
  constructor(
    private srvUser: UserService,
    private navCtrl: NavController,
    private srvFirebase: FirebaseService,
    private srvStorage: StorageService,
    private srvLoader: LoadingService,
    private srvAlert: AlertService,
    private translate: TranslateService,
    private cdh: ChangeDetectorRef
  ) { }

  ngOnInit() {
  }


  editProfile() {
    this.navCtrl.navigateForward('edit-profile');
  }

  signOut() {
    this.srvAlert.confirm({ message: this.translate.instant("SIGNOUT_TEXT") }).then(() => {
      // this.srvLoader.showLoading();
      this.srvFirebase.signOut().then(() => {
        this.srvStorage.claerAll();
        this.srvLoader.hideLoading();
        this.navCtrl.navigateRoot('phone-auth');
      });
    }).catch(() => { });
  }

  paymentMethods() {
    this.navCtrl.navigateForward('credit-cards');
  }


  raiseaComplaint() {
    this.navCtrl.navigateForward('raise-a-complaint');
  }

  changeLanguage() {
    this.navCtrl.navigateForward('change-language');
  }

  totalOrdersPage() {
    this.navCtrl.navigateForward('total-orders');
  }

}
