import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { SharedServiceService } from 'src/app/services/shared-service.service';
import { UserService } from 'src/app/services/user.service';
import * as _ from 'lodash';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.page.html',
  styleUrls: ['./product-detail.page.scss'],
})
export class ProductDetailPage implements OnInit {
  product: IProducts = {};
  user: IUser;
  shoppingCart: IShoppingCart;
  isAlreadyInCart: boolean;
  constructor(
    private activatedRoute: ActivatedRoute,
    private navCtrl: NavController,
    private srvShared: SharedServiceService,
    private userService: UserService,
    private commonService: CommonService
  ) {
    this.activatedRoute.queryParams.subscribe((params) => {
      // console.log(JSON.stringify(params.product))
      if (params && params.product) {
        this.product = params.product;
      }
    })
  }

  ngOnInit() {
    this.user = this.userService.get();
    this.shoppingCart = this.user.shoppingCart || { products: [] };
    let index = _.findIndex(this.shoppingCart.products, ['productId', this.product.id]);
    if (index > -1) {
      this.isAlreadyInCart = true;
    }
  }

  goBack() {
    this.navCtrl.pop();
  }

  grabIt() {
    let index = _.findIndex(this.shoppingCart.products, ['productId', this.product.id]);
    if (index > -1) {
      this.commonService.showMessage({ message: "Product already in cart" })
      return;
    }
    this.shoppingCart.products.push({
      productId: this.product.id,
      productName: this.product.name,
      description: this.product.description,
      productPrice: this.product.price,
      quantity: 1,
      productImage: this.product.images && this.product.images[0] ? this.product.images[0] : null
    });
    this.shoppingCart.totalAmount = this.shoppingCart.totalAmount ? (this.shoppingCart.totalAmount + this.product.price) : this.product.price;
    this.userService.set({ shoppingCart: this.shoppingCart });
    this.isAlreadyInCart = true;
  }

}
