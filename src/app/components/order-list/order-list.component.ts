import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';

declare var google: any;
@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss'],
})
export class OrderListComponent implements OnInit {

  @Input() order: IOrder = null;
  @Input() isCustomerCalled: boolean = false;
  @Output() trackNow: EventEmitter<boolean> = new EventEmitter();

  constructor(
    private navCtrl: NavController
  ) { }

  ngOnInit() {
    console.log("order:", this.order);
  }

  goToDetails(order) {
    let params: NavigationExtras = {
      queryParams: {
        order: order,
        isTracking: this.isCustomerCalled
      }
    }
    this.navCtrl.navigateForward('driver-main', params);
  }

  // trackNowAction() {
  //   this.trackNow.emit(true);
  // }

}
