import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  userInfo: IUser;
  constructor(
    public alertController: AlertController
  ) { }

  async show(message: string, header?: string) {
    const alert = await this.alertController.create({
      header: header ? header : 'Mraseelk',
      subHeader: '',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

  confirm(options: IConfirmAlerts) {
    return new Promise(async (resolve, reject) => {
      let alert = await this.alertController.create({
        header: options.header ? options.header : 'Mraseelk',//options.header,
        message: options.message,
        buttons: [
          {
            text: options.cancelBtnText || 'Cancel',
            role: 'cancel',
            // cssClass: 'secondary',
            handler: () => {
              reject(false);
            }
          }, {
            text: options.confirmBtnText || 'Okay',
            handler: () => {
              resolve(true);
            }
          }
        ]
      });
      await alert.present();
    });
  }

}
