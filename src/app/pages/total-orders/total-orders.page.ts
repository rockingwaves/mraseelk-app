import { Component, OnDestroy, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import * as _ from 'lodash';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { SharedServiceService } from 'src/app/services/shared-service.service';

@Component({
  selector: 'app-total-orders',
  templateUrl: './total-orders.page.html',
  styleUrls: ['./total-orders.page.scss'],
})
export class TotalOrdersPage implements OnInit, OnDestroy {
  myTotalOrders: number = 0;
  totalAmount: number = 0;
  completedOrders: any = [];

  private $OrderEmit: Subscription;
  constructor(
    private srvFirebase: FirebaseService,
    private navCtrl: NavController,
    private srvShared: SharedServiceService
  ) {
    // this.getTotalOrders();
    this.getProductOrders();
  }
  ngOnDestroy(): void {
    this.$OrderEmit.unsubscribe();
  }




  getProductOrders() {
    this.srvFirebase.getProductOrders((resp: ILocalResp) => {
      // console.log("getProductOrders resp: ", resp.message);
      switch (resp.status) {
        case "OK":
          this.completedOrders = [];
          this.completedOrders = resp.message; //_.filter(resp.message, (order: IShoppingCart) => { return order.status == 'completed' }).reverse();
          // this.runningOrders = _.filter(resp.message, (order: IShoppingCart) => { return (order.status != 'completed' && !order.isCancelledByAdmin && !order.isCancelledByUser) }).reverse();
          break;

        case 'ERROR':
          break;

        default:
          break;
      }
      // this.isLoading = false;
    });
  }

  getTotalOrders() {
    this.srvFirebase.getProductOrders((resp: ILocalResp) => {
      switch (resp.status) {
        case "OK":
          this.myTotalOrders = resp.message.length;
          this.totalAmount = _.sumBy(resp.message, (tO: any) => { return tO.totalAmount });
          console.log(this.totalAmount)
          break;

        default:
          break;
      }
    })
  }

  ngOnInit() {
    this.$OrderEmit = this.srvShared.$getAllProductOrders.subscribe(() => {

      this.getProductOrders();
    });

  }

  goToDetail(order) {
    console.log("i am here");
    let params: NavigationExtras = {
      queryParams: {
        order: JSON.stringify(order)
      }
    }
    this.navCtrl.navigateForward('product-order-detail', params);
  }


}
