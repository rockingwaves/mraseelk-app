import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { PhotoService } from 'src/app/services/photo-service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { LoadingService } from 'src/app/services/loading.service';
import { AlertService } from 'src/app/services/alert.service';
import { ModalController, NavController } from '@ionic/angular';
import { SharedServiceService } from 'src/app/services/shared-service.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  user: IUser = this.srvUser.get();
  private replicaUser = this.user;
  isDriver: boolean = this.user.userType == 'driver';
  constructor(
    private srvUser: UserService,
    private srvPhoto: PhotoService,
    private srvFirebase: FirebaseService,
    private srvLoader: LoadingService,
    private srvAlert: AlertService,
    private navCtrl: NavController,
    private srvShared: SharedServiceService
  ) { }

  ngOnInit() {
  }

  goBack() {
    this.navCtrl.back();
  }

  addPhoto() {
    this.srvPhoto.addPhoto((photo: string) => {
      this.user.photoUrl = photo;
    });
  }


  update() {
    if (this.user.photoUrl !== this.replicaUser.photoUrl) {
      this.uploadPhoto();
    } else if (this.user.displayName !== this.replicaUser.displayName) {
      this.updateUser();
    } else {
      this.goBack();
    }
  }

  uploadPhoto() {
    this.srvLoader.showLoading();
    this.srvFirebase.uploadImageToFirebase(this.user.photoUrl).then((url: string) => {
      this.user.photoUrl = url;
      this.updateUser();
      this.srvLoader.hideLoading();
      this.goBack();
    }).catch((err) => {
      this.srvAlert.show(err);
      this.srvLoader.hideLoading();
    })
  }

  updateUser() {
    this.srvUser.set(this.user);
    let _user = {
      photoUrl: this.user.photoUrl ? this.user.photoUrl : null,
      displayName: this.user.displayName
    }
    this.srvFirebase.updatUserInfo(this.user, _user).then(() => {
      this.srvShared.showToast("USER_UPDATED_SUCCESFULLY");
    }).catch((err) => {
      this.srvAlert.show(err.message);
    })

  }

}
