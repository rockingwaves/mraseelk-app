import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { FCM } from '@ionic-native/fcm/ngx';
import { Push } from '@ionic-native/push/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { SupplantPipe } from './pipes/supplant.pipe';


export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


import { SelectCountryPage } from '../app/modals/select-country/select-country.page';
import { CreateOrderPage } from '../app/modals/create-order/create-order.page';
import { SelectLocationPage } from '../app/modals/select-location/select-location.page';
import { OrdersPage } from './driver/modals/orders/orders.page';
import { UserDetailDuringOrderPage } from '../app/modals/user-detail-during-order/user-detail-during-order.page';
import { ChatViewPage } from '../app/modals/chat-view/chat-view.page';

@NgModule({
  declarations: [AppComponent, SelectCountryPage, CreateOrderPage, SelectLocationPage, OrdersPage, UserDetailDuringOrderPage, ChatViewPage],
  entryComponents: [SelectCountryPage, CreateOrderPage, SelectLocationPage, OrdersPage, UserDetailDuringOrderPage, ChatViewPage],
  imports: [
    BrowserModule, IonicModule.forRoot(), AppRoutingModule,
    HttpClientModule, // <--- add this
    FormsModule,
    TranslateModule.forRoot({ // <--- add this
      loader: { // <--- add this 
        provide: TranslateLoader, // <--- add this
        useFactory: (createTranslateLoader),  // <--- add this
        deps: [HttpClient] // <--- add this
      },
    })
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    NativeStorage,
    FirebaseAuthentication,
    SupplantPipe,
    Camera,
    Geolocation,
    Diagnostic,
    FCM,
    Push,
    CallNumber,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
