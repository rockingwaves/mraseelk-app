import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { SharedServiceService } from 'src/app/services/shared-service.service';
import { SelectCountryPage } from 'src/app/modals/select-country/select-country.page';
import { NavigationOptions } from '@ionic/angular/providers/nav-controller';

@Component({
  selector: 'app-phone-auth',
  templateUrl: './phone-auth.page.html',
  styleUrls: ['./phone-auth.page.scss'],
})
export class PhoneAuthPage implements OnInit {
  country: ICountry = {
    n: "Pakistan",
    i: "pk",
    d: "92"
  }

  userNumber: string = '';
  constructor(
    private navCtrl: NavController,
    private srvShared: SharedServiceService,
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {
  }

  proceedNext() {
    if (!this.userNumber.trim().length) { this.srvShared.showToast("PLEASE_ENTER_CORRECT_NUM"); return; }
    let correctNum: string = '+' + this.country.d + this.userNumber;
    this.srvShared.showAlert("NUMBER_IS_CORRECT", { number: correctNum }).then(() => {
      let params: NavigationOptions = {
        queryParams: {
          phoneNumber: correctNum
        }
      }
      console.log(params)
      this.navCtrl.navigateForward('phone-verification', params);
    })
  }


  async selectCountry() {
    let modal = await this.modalCtrl.create({ component: SelectCountryPage });
    modal.present();

    modal.onDidDismiss().then((data) => {
      if (data.data) this.country = data.data;
    })
  }

  goBack() {
    this.navCtrl.pop();
  }
}
