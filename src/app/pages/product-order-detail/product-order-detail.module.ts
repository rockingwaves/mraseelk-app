import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductOrderDetailPageRoutingModule } from './product-order-detail-routing.module';

import { ProductOrderDetailPage } from './product-order-detail.page';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    PipesModule,
    IonicModule,
    ProductOrderDetailPageRoutingModule
  ],
  declarations: [ProductOrderDetailPage]
})
export class ProductOrderDetailPageModule {}
