import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LocationService } from 'src/app/services/location.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {

  constructor(
    private navCtrl: NavController,
    private srvLocation: LocationService
  ) { }

  ngOnInit() {
    // this.navCtrl.navigateForward('select-location');
    if (!window.isMobileWeb) this.selectLocPage();
  }

  selectLocPage() {
    this.srvLocation.getLocationAuthorizationStatus().then(status => {
      switch (status) {
        case "OK":
          // 
          break;

        case "ERROR":
          this.navCtrl.navigateForward('select-location');
          break;

        default:
          break;
      }

    });
  }

  continue() { this.navCtrl.navigateForward('phone-auth'); }

}
