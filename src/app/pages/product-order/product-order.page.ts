import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { SharedServiceService } from 'src/app/services/shared-service.service';
import { Constant } from 'src/app/services/constants.service';
import { LocationService } from 'src/app/services/location.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { LoadingService } from 'src/app/services/loading.service';
import { AlertService } from 'src/app/services/alert.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-product-order',
  templateUrl: './product-order.page.html',
  styleUrls: ['./product-order.page.scss'],
})
export class ProductOrderPage implements OnInit {
  product: IProducts = null;
  deliveryCharges: number = 0;
  quantity: number = 1;

  productOrder: IProductsOrder = {
    // deliveryLatlng: {},
    productId: '',
    productName: '',
    productPrice: 0,
    quantity: 1,
    // specialInstruction: '',
    // totalAmount: null,
    // orderId: ''
  }
  constructor(
    private navCtrl: NavController,
    private activateRoute: ActivatedRoute,
    private srvLoc: LocationService,
    private srvFirebase: FirebaseService,
    private srvLoader: LoadingService,
    private srvAlert: AlertService,
    private translate: TranslateService
  ) {
    this.activateRoute.queryParams.subscribe((param) => {
      this.product = param.product;
      this.productOrder.productId = this.product.id;
      this.productOrder.productName = this.product.name;
      this.productOrder.productPrice = this.product.price

    });
    // this.getCurrentLoc();
  }

  ngOnInit() {
  }

  goBack() {
    this.navCtrl.pop();
  }

  getCurrentLoc() {
    this.srvLoc.getCurrentLocation().then((resp) => {
      // this.productOrder.deliveryLatlng = resp.coords;
      let distance = this.srvLoc.distance(Constant.appValues.storeLatitude, Constant.appValues.storeLongitude, resp.coords.latitude, resp.coords.longitude, 'K').toFixed(0);
      this.deliveryCharges = parseFloat(distance) > 5 ? parseFloat(distance) * Constant.appValues.perKM : Constant.appValues.till5KM;
      this.calculateTotalAmount();
      console.log(this.deliveryCharges);
    });
  }

  Order() {
    // // this.productOrder.orderId = "id-" + new Date().getTime();
    // this.srvLoader.showLoading();
    // this.srvFirebase.addProductOrder(this.productOrder, (resp: ILocalResp) => {
    //   switch (resp.status) {
    //     case "OK":
    //       this.srvAlert.show(this.translate.instant("YOUR_ORDER_HAS_BEEN_PLACED"), this.translate.instant('THANK_YOU'));
    //       this.navCtrl.navigateBack('tabs/tabs/tab2');
    //       this.srvLoader.hideLoading();
    //       break;

    //     case "ERROR":
    //       this.srvLoader.hideLoading();
    //       this.srvAlert.show(resp.message);
    //       break;

    //     default:
    //       break;
    //   }
    // })
    // console.log(this.productOrder);

  }

  incrQty() {

    if (this.quantity >= this.product.quantity) { return; }
    this.quantity += 1;
    this.calculateTotalAmount();
  }

  decrQty() {
    if (this.quantity <= 1) { return; }
    this.quantity--;
    this.calculateTotalAmount();
  }




  calculateTotalAmount() {
    // this.productOrder.totalAmount = this.deliveryCharges + (this.quantity * parseFloat(this.product.price));
  }


}
