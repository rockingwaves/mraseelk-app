import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PhoneAuthPageRoutingModule } from './phone-auth-routing.module';

import { PhoneAuthPage } from './phone-auth.page';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PhoneAuthPageRoutingModule,
    TranslateModule,
    PipesModule
  ],
  declarations: [PhoneAuthPage]
})
export class PhoneAuthPageModule { }
