import { Injectable } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';


@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  PHOTO_QUALITY: any = 60;
  PHOTO_TARGET_WIDTH: any = 512;
  PHOTO_TARGET_HEIGHT: any = 512;
  constructor(
    private actionSheetController: ActionSheetController,
    private translate: TranslateService,
    private camera: Camera,
  ) { }

  async addPhoto(callback: (image: string) => void) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Albums',
      buttons: [{
        text: this.translate.instant("TAKE_PHOTO"),
        handler: () => {
          // console.log('Play clicked');
          this.takePhoto({ sourceType: this.camera.PictureSourceType.CAMERA }, (photos) => {
            callback && callback(photos);
          });
        }
      }, {
        text: this.translate.instant("BROWSE_ALBUM"),
        handler: () => {
          this.takePhoto({ sourceType: this.camera.PictureSourceType.PHOTOLIBRARY }, (photos) => {
            callback && callback(photos);
          });
        }
      }, {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }


  takePhoto(_options: CameraOptions, callback: (image: string) => void) {
    const options: CameraOptions = {
      quality: this.PHOTO_QUALITY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetHeight: this.PHOTO_TARGET_HEIGHT,
      targetWidth: this.PHOTO_TARGET_WIDTH,
      sourceType: _options.sourceType ? _options.sourceType : this.camera.PictureSourceType.PHOTOLIBRARY,
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      callback && callback(base64Image);

    }, (err) => {
      // Handle error
    });
  }

  // selectPictures(options: ImagePickerOptions, callback: (image: string[]) => void) {
  //   options.quality = this.PHOTO_QUALITY;
  //   options.height = 1024 * 0.5;
  //   options.width = 1024 * 0.5;
  //   options.outputType = 1;
  //   this.imagePicker.getPictures(options).then((results) => {
  //     callback && callback(results);
  //     // for (var i = 0; i < results.length; i++) {
  //     //   console.log('Image URI: ' + results[i]);
  //     // }
  //   }, (err) => {
  //     console.log("err", err);
  //   });
  // }

}
