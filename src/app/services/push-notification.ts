import { Injectable } from '@angular/core';
import { Push, PushOptions, PushObject } from '@ionic-native/push/ngx'
import { FirebaseService } from './firebase.service';
import { UserService } from './user.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { FCM } from '@ionic-native/fcm/ngx';
@Injectable({
    providedIn: 'root'
})

export class PushNotificationService {

    constructor(
        private push: Push,
        private srvFirebase: FirebaseService,
        private srvUser: UserService,
        private http: HttpClient,
        // private fcm: FCM
    ) { }

    init() {

        // this.fcm.getToken().then((res) => {
        //     console.log("fcm.getToken ", res);
        // }).catch((err) => {
        //     console.log("fcm.getToken err ", err);

        // })

        const options: PushOptions = {
            android: {
                senderID: '673968461626'
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'false'
            },
            // windows: {},
            // browser: {
            //     pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            // }
        }

        const pushObject: PushObject = this.push.init(options);

        console.log("pushObject", pushObject);

        pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));

        pushObject.on('registration').subscribe((registration) => {
            console.log('Device registered', registration.registrationId);

            let user = this.srvUser.get();
            console.log("what is user type? ", user.userType);
            if (user.phoneNumber) {
                user.userType == 'driver' ? this.srvFirebase.updatDriverInfo(user, { deviceToken: registration.registrationId }) : this.srvFirebase.updatUserInfo(this.srvUser.get(), { deviceToken: registration.registrationId });
                user.deviceToken = registration.registrationId;
                user.permissions.hasNotificationPermission = true;
                this.srvUser.set(user);
            }
        });

        pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));


        // this.push.init()
        // this.fcm.getToken().then((token) => {
        //     console.log("fcm tokem: ", token);
        //     let user = this.srvUser.get();
        //     if (user.phoneNumber) {
        //         user.userType == 'driver' ? this.srvFirebase.updatDriverInfo(this.srvUser.get(), { deviceToken: token }) : this.srvFirebase.updatUserInfo(this.srvUser.get(), { deviceToken: token });
        //     }
        //     this.onNotification();
        // }).catch((err) => { console.log("FCM ERROR", err) });
    }

    // onNotification() {
    //     this.fcm.onNotification().subscribe((notifcation) => {
    //         //   alert(JSON.stringify(notifcation));
    //     }, err => {
    //         console.log(err);
    //     });
    // }


    sendPush(pushData: IPushData) {
        if (!pushData.token) return;
        // let header = new HttpHeaders();
        // header.append("Content-Type", "application/json");
        // header.append("Authorization", "key=AAAAnOumlzo:APA91bG-VYlbfDAC4-I5aruyrmSjatT0wBb2s2nyg5jo7-5sNTVqUiutAOrGAlr4oNJWF7R6sSrVTl1lQ3Y0nKYLTvViVVIqwgHwz01Pe5ptbcXM1w9N6HLhsIoEMMmyitJhzhhUC5dX");

        // let body = {
        //     "to": pushData.deviceToken,
        //     "notification": {
        //         "title": pushData.title,
        //         "body": pushData.bodyText,
        //         "mutable_content": true,
        //         "sound": "Tri-tone"
        //     },
        //     "data":
        //     {
        //         "hello": pushData.bodyText,
        //     }
        // }
        // this.http.post('https://fcm.googleapis.com/fcm/send', body, { headers: header }).subscribe(() => { });

        pushData.pushData = JSON.stringify({});

        this.http.get(`https://us-central1-mraseelk-5f2ec.cloudfunctions.net/sendPushNotification?dest=${JSON.stringify(pushData)}`).subscribe(() => { });


    }

}

