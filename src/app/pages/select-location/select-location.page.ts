import { Component, OnInit } from '@angular/core';
import { LocationService } from 'src/app/services/location.service';
import { NavController } from '@ionic/angular';
import { AlertService } from 'src/app/services/alert.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-select-location',
  templateUrl: './select-location.page.html',
  styleUrls: ['./select-location.page.scss'],
})
export class SelectLocationPage implements OnInit {

  constructor(
    private srvLoc: LocationService,
    private navCtrl: NavController,
    private srvAlert: AlertService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
  }

  goBack() { this.navCtrl.pop(); }

  enableLocation() {
    this.srvLoc.getCurrentLocation().then((getCurrentLocation) => {
      // this.navCtrl.navigateForward('phone-auth');
      this.navCtrl.pop();
    }).catch(() => {
      // this.navCtrl.pop();
      this.srvAlert.show(this.translate.instant("LOCATION_REQUIRED_TEXT"));
    });
  }

}
