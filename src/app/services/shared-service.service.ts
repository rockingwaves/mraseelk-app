import { Injectable, EventEmitter } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { FirebaseService } from './firebase.service';
import { StorageService } from './storage.service';
import { Keyboard, KeyboardResizeMode } from '@ionic-native/keyboard/ngx';
import { TranslationService } from './translation.service';
import { ToastService } from './toast.service';
import { AlertService } from './alert.service';
import { CreateOrderPage } from '../modals/create-order/create-order.page';
import { SelectLocationPage } from '../modals/select-location/select-location.page';
import { NavigationExtras } from '@angular/router';

import * as geolib from 'geolib';
import { GeolibInputCoordinates } from 'geolib/es/types';
import { CoversationService } from './coversation.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class SharedServiceService {
  public $orderAdded: EventEmitter<boolean> = new EventEmitter();
  public $getAllProductOrders: EventEmitter<boolean> = new EventEmitter();
  public isKeyboardOpen: boolean = false;
  constructor(
    private translate: TranslationService,
    private srvToast: ToastService,
    private srvAlert: AlertService,
    private srvFirebase: FirebaseService,
    private srvStorage: StorageService,
    private plgKeyboard: Keyboard,
    private modalCtrl: ModalController,
    private srvMessages: CoversationService,
    private srvUser: UserService

  ) { }


  initFirebase() {
    this.srvFirebase.initFirebase();
  }


  showToast(value: string, args?: Object) {
    this.translate.transform(value, args).then((res: string) => {
      this.srvToast.show({ message: res });
    });
  }


  showAlert(value: string, args?: Object) {
    return new Promise((resolve: (val?: any) => void) => {
      this.translate.transform(value, args).then((res: string) => {
        this.srvAlert.confirm({
          message: res
        }).then(() => {
          resolve();
        }).catch(() => { });
      });
    });
  }


  userLogout(callback: Function) {
    this.srvFirebase.signOut().then(() => {
      this.srvStorage.claerAll().then(() => {
        callback();
      });
    });
  }

  ValidateEmail(email: string) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  validateName(name: string) {
    var regex = /^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+$/g
    return regex.test(String(name).toLowerCase());
  }

  public keyboard = {

    closeKeyboard: (callback?: Function, callbackTimeout?: number) => {
      console.log("Shared Service: Keyboard: Close: isOpen: ", this.isKeyboardOpen);
      if (this.isKeyboardOpen) {
        this.plgKeyboard.hide();
        setTimeout(() => {
          console.log("Shared Service: Keyboard: Closed: Now isOpen?: ", this.isKeyboardOpen);
          if (callback) callback();
        }, callbackTimeout || 350);
      }
      else if (callback) callback();

    },


    resizeNative: () => { this.plgKeyboard.setResizeMode(KeyboardResizeMode.Native); },
    resizeBody: () => { this.plgKeyboard.setResizeMode(KeyboardResizeMode.Body); },
    resizeNone: () => { this.plgKeyboard.setResizeMode(KeyboardResizeMode.None); },
    resizeIonic: () => { this.plgKeyboard.setResizeMode(KeyboardResizeMode.Ionic) },
    hideFormAccessoryBar: (hide: boolean) => { this.plgKeyboard.hideFormAccessoryBar(hide); }
  }


  asyncLoop(length: number, onIteration: (loop: IAsyncLoop) => void, onCompletion?: Function) {
    let index = -1;
    let done = false;
    let loop: IAsyncLoop = {
      next: () => {
        if (done) return;
        if (index < length - 1) {
          index++;
          onIteration(loop);
        } else {
          done = true;
          if (onCompletion) { onCompletion("finish"); }
        }
      },
      getIndex: () => { return index; },
      break: () => {
        done = true;
        if (onCompletion) { onCompletion("break"); }
      }
    };
    loop.next();
  }

  async addOrderModal(_routerOutler: any, callback: (status: string) => void) {
    let modal = await this.modalCtrl.create({
      component: CreateOrderPage,
      presentingElement: _routerOutler.nativeEl
    });
    modal.present();
    modal.onDidDismiss().then((data) => {
      callback && callback(data.data.status);
    });

  }

  async selectLocation(data: ILatLng, callback) {
    let modal = await this.modalCtrl.create({
      component: SelectLocationPage,
      componentProps: { latLng: data }
    });
    modal.present();

    modal.onDidDismiss().then((data) => {
      console.log(data.data);
      if (data.data) {
        callback && callback(data.data);
      }
    })
  }


  createParams(propName: string, value: any) {
    let params: NavigationExtras = {
      queryParams: {
        [propName]: value
      }
    }
    return params;
  }

  // getDistance(startLatLng: ILatLng, endsLatLng: ILatLng) {
  //   console.log("geolib", geolib);
  //   if (!geolib) return;
  //   console.log(startLatLng);
  //   console.log(endsLatLng);
  //   console.log("geolib", geolib.getDistance(startLatLng, endsLatLng));
  //   return geolib.getDistance(startLatLng, endsLatLng);
  // }

  getDistance(from: GeolibInputCoordinates, to: GeolibInputCoordinates) {
    let distance = 0.001 * geolib.getDistance(from, to);  //meter to KM   
    // console.log('distance: ', distance);
    return distance;
  }


  createNewChat(user: IChatUser) {
    let conv: IChatInit = {
      chatId: this.srvMessages.createChatID(this.srvUser.get().phoneNumber, user.userId),
      chatContainIDs: [this.srvUser.get().phoneNumber, user.userId],
      created: this.srvMessages.timestamp(),
      creator: this.srvMessages.creator(),
      users: [this.srvMessages.creator(), user],
      updated: this.srvMessages.timestamp(),
      typing: null,
      lastMessage: null,
      isGroup: false,
      title: user.username
    }
    this.srvMessages.init();
    return conv;
  }

}
