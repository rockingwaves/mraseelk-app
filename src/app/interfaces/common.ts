
interface Window {
    isMobileWeb: boolean;
    platform: string;
    moment: any;
    plugins: any;
}

interface IUser {
    isLoggedIn?: boolean;
    registeredDate?: any;
    updatedDate?: any;
    platform?: string;
    phoneNumber?: string;
    location?: ILatLng;
    deviceToken?: string;
    latitude?: number;
    longitude?: number;
    email?: string;
    photoUrl?: string;
    displayName?: string;
    password?: string;
    isAgreementSigned?: boolean;
    gender?: string;
    permissions?: IPermissions;
    userType?: string; //driver,user
    shoppingCart?: IShoppingCart;
    rating?: string;
    status?: any;
}

interface IPermissions {
    haslocationPermission: boolean;
    hasNotificationPermission: boolean;
}

interface IToastOption {
    message?: string,
    duration?: any,
    position?: any,
    showCloseButton?: boolean,
    closeButtonText?: string,
    cssClass?: string,
}

interface IConfirmAlerts {
    header?: string;
    message: string,
    confirmBtnText?: string,
    cancelBtnText?: string,
}


interface IGeoLocation {
    latitude: number;
    longitude: number;
    accuracy?: number;
    altitude?: number,
    altitudeAccuracy?: number
    heading?: number
    speed?: number;
}

interface ILatLng {
    latitude: any;
    longitude: any;
}

interface ICountry {
    n: string;
    i: string;
    d: string;
}

interface IHistory {
    id?: string;
    imageURL?: string;
    quote?: string;
    phoneNumber?: string;
    dateAdded?: string;
}

interface IPushNotification {
    latitude: number;
    longitude: number;
    radius?: number;
    token?: string;
    message?: string;
    pushData?: string;
}


interface ILocalResp {
    status: string;
    message: any
}


interface IFaceboolLogin {
    email: string;
    id: string;
    last_name: string;
    picture: {
        data: {
            is_silhouette: boolean;
            height: number;
            url: string;
            width: number
        }
    }
    name: string;
    first_name: string;
}


interface IAddBookDetail {
    images: IPics[];
    price: string;
    title: string;
    ISBN: string;
    instituteName: string;
    condition: 'used' | 'new';
    underlineWith: 'pencil' | 'outliner';
    underlineQutantity: 'none' | 'few' | 'some' | 'many';
    description: string;
    isVideoWatched: boolean;
    addedBy: string;
    savedBy: string[]
}

interface IPics {
    mediaUrl: string;
    isUploaded: boolean;
}

interface ISelectPics {
    maximumImagesCount?: number,
    width?: number,
    height?: number,
    quality?: number,
    outputType?: number

}

interface IAsyncLoop {
    /**
    * @description
    * Call loop.next() after your synchronus task completion.
    */
    next: () => void;
    /**
    * @description
    * Call this method to get current loop index.
    */
    getIndex: () => number;
    /**
    * @description
    * Call loop.break() to terminate iterations.
    */
    break: () => void;
}

interface ISmsInfo {
    verificationId: string;
    smsCode: string;
}


interface ISelectType {
    label: string;
    isSelected: boolean
}

interface IRespGoogleGeocode {
    address_components: Addresscomponent[];
    formatted_address: string;
    geometry: Geometry;
    place_id: string;
    types: string[];
}


interface Geometry {
    bounds: Bounds;
    location: Location;
    location_type: string;
    viewport: Bounds;
}

interface Location {
    lat: number;
    lng: number;
}

interface Bounds {
    east: number;
    north: number;
    south: number;
    west: number;
}

interface Addresscomponent {
    long_name: string;
    short_name: string;
    types: string[];
}

interface ILatLng {
    latitude: any;
    longitude: any;
    accuracy?: number
}

interface IPDLocs {
    pickupLocation: string;
    pickupLatLng: ILatLng;
    dropoffLocation: string;
    dropoffLatLng: ILatLng;
    orderDetail: string;
}

interface IAddress {
    address1: string;
    address2: string;
    city: string;
    state: string;
    zipCode: string;
    country: string;
    accuracy?: number
}


interface IOrder {
    userName?: string;
    userId?: string;
    user?: IUser;
    userPhotoUrl?: string;
    status?: 'pending' | 'accepted' | 'arrived' | 'started' | 'completed';
    typeofService?: string;
    typeofCar?: string;
    pickupLoc?: {
        address: string;
        latlng: IGeoLocation
    };
    dropoffLoc?: {
        address: string;
        latlng: IGeoLocation
    };
    comment?: string;
    addedAt?: any;
    driverOffer?: IDriverOffer;
    orderId?: string;
    totalFares?: number;
    userDeviceToken?: string;
    isCustomerRated?: boolean;
    isDriverRated?: boolean;
    orderPriceOffer?: {
        price: string, descp: string;
    };
    orderPriceStatus?: 'requested' | 'accepted' | 'rejected' | 'request_to_next_driver'
}

interface IDriverOffer {
    driverName?: string;
    driverLoc?: IAddressLatlng;
    phoneNumber?: string;
    deviceToken?: string;
    rating?: string
}

interface IAddressLatlng {
    address: IAddress;
    latlng: IGeoLocation
}

interface ICategory {
    dateAdded: string
    dateUpdated: string
    id: string
    imageUrl: string
    name: string
    isSelected: boolean
}

interface IProducts {
    categoryId?: string;
    dateAdded?: string;
    dateUpdated?: string;
    description?: string;
    id?: string;
    name?: string;
    price?: number;
    quantity?: number;
    status?: "available" | 'not_available',
    images?: string[]

}

interface IAppValue {
    till5KM?: number;
    perKM?: number;
    storeLatitude?: number;
    storeLongitude?: number;
    driverPerkM: number;
    drivertillKm: number;
}

interface IProductsOrder {
    productId?: string;
    productName?: string;
    productImage?: string;
    productPrice?: number;
    quantity?: number;
    description?: string;
    specialInstruction?: string
}

interface IShoppingCart {
    products?: IProductsOrder[];
    totalAmount?: number;
    deliveryFee?: number;
    deliveryLatlng?: ILatLng;
    userId?: string;
    orderId?: string;
    specialInstruction?: string;
    status?: string;
    isCancelledByAdmin?: boolean;
    isCancelledByUser?: boolean;
    dateAdded?: string;
}

interface IComplain {
    id?: string;
    message?: string;
    userPhone?: string;
    status?: string;  //pending, replied
    dateAdded?: string;
}

interface IPushData {
    token: string;
    title: string;
    bodyText: string;
    type: string;
    pushData?: any
}


interface IChatInit {
    chatId: string;
    chatContainIDs: string[];
    created: any;
    creator: IChatUser;
    users: IChatUser[];
    updated: any;
    typing: IChatTyping;
    lastMessage: IChatMsg;
    isGroup: boolean;
    chatListInfo?: IChatInfo;
    title?: string;

}

interface IChatInfo {
    thumb: string;
    chatName: string;
    unread_count: number;
}

interface IChatUser {
    userId?: string;
    username?: string;
    isCreator?: boolean;
    thumb?: string;
    device_token?: string;
    unread_count?: number;
}

interface IChatTyping {
    status?: boolean;
    user?: IChatUser;
}

interface IChatMsg {
    content: string;
    created: any;
    delivered: boolean;
    id?: any;
    seen: boolean;
    type: 'text' | 'media',
    user: IChatUser;
    isMine: boolean;
    timeFormatted: any;

}


interface IChatState {
    customKeyboard: any;
    receivedKeyboardData: any;
    keyboardRef: any;
    scrollViewRef: any
    headerTitle: string;

}

interface IMsgsListState {
    data: IChatInit[],

}