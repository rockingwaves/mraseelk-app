import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { SharedServiceService } from 'src/app/services/shared-service.service';
import { NavController, IonRouterOutlet } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  user: IUser = this.srvUser.get();
  constructor(
    private srvUser: UserService,
    private navCtrl: NavController,
    private srvShared: SharedServiceService,
    private _routerOutler: IonRouterOutlet,
    private srvLoader: LoadingService
  ) { }

  ngOnInit() {
    // setTimeout(() => {
    //   let order = {"userId":"+923465566811","status":"completed","orderId":"id-1594063695680","dateAdded":"Tue, Jul 7, 2020 12:28 AM","deliveryFee":0,"totalAmount":10,"products":[{"productName":"What is Lorem Ipsum?","productId":"id-1592641232694","description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.","productPrice":10,"productImage":"https://firebasestorage.googleapis.com/v0/b/mraseelk-5f2ec.appspot.com/o/1592641230?alt=media&token=13ceb69c-ec86-4a44-a55e-a480665b8538","quantity":1}]};
    //   let params: NavigationExtras = {
    //     queryParams: {
    //       order: JSON.stringify(order)
    //     }
    //   }
    //   this.navCtrl.navigateForward('product-order-detail', params);
    // }, 1500);
  }

  addOrder() {
    this.srvShared.addOrderModal(this._routerOutler, (data) => {
      // console.log(data);
      this.srvLoader.hideLoading();
      if (data == 'OK') {
        this.navCtrl.navigateForward('tabs/tabs/tab4')
      }
    });
  }




}
