import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RaiseAComplaintPageRoutingModule } from './raise-a-complaint-routing.module';

import { RaiseAComplaintPage } from './raise-a-complaint.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RaiseAComplaintPageRoutingModule,
    TranslateModule
  ],
  declarations: [RaiseAComplaintPage]
})
export class RaiseAComplaintPageModule { }
