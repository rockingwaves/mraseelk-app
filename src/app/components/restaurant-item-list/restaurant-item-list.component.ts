import { Component, OnInit, Input } from '@angular/core';
import { NavController } from '@ionic/angular';
import { SharedServiceService } from 'src/app/services/shared-service.service';

@Component({
  selector: 'app-restaurant-item-list',
  templateUrl: './restaurant-item-list.component.html',
  styleUrls: ['./restaurant-item-list.component.scss'],
})
export class RestaurantItemListComponent implements OnInit {

  @Input() product: IProducts;

  constructor(
    private navCtrl: NavController,
    private srvShared:SharedServiceService
  ) { }

  ngOnInit() {
  }

  goToDetails() {
    this.navCtrl.navigateForward('product-detail',this.srvShared.createParams('product',this.product));
  }

}
