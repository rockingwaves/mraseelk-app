import { Injectable } from '@angular/core';

import { UserService } from './user.service';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { Geolocation, Geoposition, GeolocationOptions } from '@ionic-native/geolocation/ngx';
import { TranslationService } from './translation.service';
import { AlertService } from './alert.service';


declare var google: any;
@Injectable({
  providedIn: 'root'
})
export class LocationService {
  private location: Geoposition;
  user: IUser;
  constructor(
    private geolocation: Geolocation,
    private diagnostics: Diagnostic,
    private srvUser: UserService,
    private srvTranslation: TranslationService,
  ) { }

  getCurrentLocation(options?: GeolocationOptions): Promise<Geoposition> {
    return new Promise((resolve, reject) => {
      this.user = this.srvUser.get();
      this.location = {
        coords: {
          latitude: 0,
          longitude: 0,
          accuracy: 0.00,
          altitude: 0.00,
          altitudeAccuracy: 0.00,
          heading: 0.00,
          speed: 0.00
        },
        timestamp: parseInt(new Date().toISOString()),
      };

      if (!options) options = { enableHighAccuracy: true };
      if (window.platform == 'android') options["timeout"] = 5000;


      if (window.isMobileWeb) {
        navigator.geolocation.getCurrentPosition((location) => {
          this.location.coords.latitude = location.coords.latitude;
          this.location.coords.longitude = location.coords.longitude;
          this.user.permissions.haslocationPermission = true;
          this.srvUser.set(this.user);
          resolve(this.location);
        });
        return;
      }


      if (window.platform == 'android') {
        this.diagnostics.getLocationMode()
          .then((status) => {
            // console.log("Location Service: Get Location Mode: " + status);
            if (status != this.diagnostics.locationMode.LOCATION_OFF) {
              this.geolocation.getCurrentPosition(options).then((location) => {
                this.user.permissions.haslocationPermission = true;
                this.srvUser.set(this.user);
                // console.log("Location Service: Plugin returned location");
                this.location = location;
                resolve(this.location);
              }).catch((error) => {
              });
            }
            else {
              this.srvTranslation.transform("LOCATION_OFF_ERROR_DESC").then(() => {
                setTimeout(() => {
                  this.diagnostics.switchToLocationSettings();
                }, 100);

              }).catch(() => { });

            }
          })
          .catch((resp) => { console.log("Location Service: Get Location Mode Error: " + resp); })
      } else {
        // if (shoulTakeLocation) {
        this.getiOSLocation(options, resolve, reject);
        //   return;
        // } else {

        // }
      }
    });
  }

  getiOSLocation(options: any, resolve: Function, reject: Function) {
    console.log("i am here with option ", options);
    options.enableHighAccuracy = true;
    this.geolocation.getCurrentPosition(options).then((location) => {

      this.location = location;
      resolve(this.location);
    }).catch((error) => {
      console.log("Location Service: Plugin Error: " + typeof error + " " + JSON.stringify(error));
      reject(error);
    });
    // console.log("Location Service: Get Current Location by Plugin");

  }

  getPosition() {
    return this.location;
  }

  watchPosition() {
    return this.geolocation.watchPosition({ enableHighAccuracy: true });
  }

  clearWatchPosition(id) {
    
  }

  geocode(coords: ILatLng): Promise<{ status: string; data: { address1: string; address2: string; city: string; state: string; zipCode: string; country: string; } }> {
    return new Promise((resolve, reject) => {
      let geocoder = new google.maps.Geocoder();
      var latlng = { lat: coords.latitude, lng: coords.longitude };

      geocoder.geocode({ 'location': latlng }, function (results: IRespGoogleGeocode[], status) {
        let address = {
          address1: "",
          address2: "",
          city: "",
          state: "",
          zipCode: "",
          country: ''
        };
        if (status === 'OK') {
          let address_components = results[0].address_components;
          for (var i = 0; i < results[0].address_components.length; i++) {
            if (address_components[i].types[0] == "postal_code") {
              address.zipCode = address_components[i].long_name;
            }
            else if (address_components[i].types[0] == "administrative_area_level_1") {
              address.state = address_components[i].long_name;
            }
            else if (address_components[i].types[0] == "locality") {
              address.city = address_components[i].long_name;
            }
            else if (address_components[i].types[0] == "sublocality_level_1" || address_components[i].types[0] == "sublocality" || address_components[i].types[0] == "political") {
              address.address2 = address_components[i].long_name;
            }
            else if (address_components[i].types[0] == "premise" || address_components[i].types[0] == "route") {
              address.address1 = address_components[i].long_name;
            } else if (address_components[i].types[0] == "country" || address_components[i].types[1] && address_components[i].types[1] == "country") {
              address.country = address_components[i].long_name;
            }
          }
          resolve({ status: "OK", data: address });
        }
        else resolve({ status: "ERROR", data: null })
      });
    });
  }

  getLocationAuthorizationStatus() {
    return new Promise((resolve) => {
      if (window.isMobileWeb) { resolve("OK"); return; }
      this.diagnostics.isLocationEnabled().then((isLocationOn) => {
        if (isLocationOn) {
          this.diagnostics.isLocationAuthorized().then((isAuthorized) => {
            if (isAuthorized) {
              resolve("OK");
            } else {
              resolve("ERROR");
            }
          });
        } else {
          resolve("ERROR");
        }
      }, err => {
        resolve("ERROR");
      });
    });
  }

  distance(lat1: number, lon1: number, lat2: number, lon2: number, unit: string) {
    var radlat1 = Math.PI * lat1 / 180
    var radlat2 = Math.PI * lat2 / 180
    var theta = lon1 - lon2
    var radtheta = Math.PI * theta / 180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180 / Math.PI
    dist = dist * 60 * 1.1515
    if (unit == "K") { dist = dist * 1.609344 }
    if (unit == "N") { dist = dist * 0.8684 }
    return dist
  }

}
