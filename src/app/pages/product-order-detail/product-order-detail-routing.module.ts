import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductOrderDetailPage } from './product-order-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ProductOrderDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductOrderDetailPageRoutingModule {}
