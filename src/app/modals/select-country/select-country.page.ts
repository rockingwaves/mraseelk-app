import { Component, OnInit } from '@angular/core';
import { Constant } from 'src/app/services/constants.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-select-country',
  templateUrl: './select-country.page.html',
  styleUrls: ['./select-country.page.scss'],
})
export class SelectCountryPage implements OnInit {
  countries: ICountry[] = Constant.countriesCode;

  constructor(
    private modal: ModalController
  ) { }

  ngOnInit() {
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.countries = Constant.countriesCode;
    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.countries = this.countries.filter((item) => {
        return (item.n.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  dismiss(country: ICountry) {
    this.modal.dismiss(country);
  }

  goBack() {
    this.modal.dismiss();
  }

}
