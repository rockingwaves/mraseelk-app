import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { NavController, IonSlides } from '@ionic/angular';
import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';
import { ActivatedRoute } from '@angular/router';
import { FirebaseService } from 'src/app/services/firebase.service';
import { SharedServiceService } from 'src/app/services/shared-service.service';
import { LoadingService } from 'src/app/services/loading.service';
import { UserService } from 'src/app/services/user.service';
import * as moment from 'moment';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { PushNotificationService } from 'src/app/services/push-notification';

@Component({
  selector: 'app-phone-verification',
  templateUrl: './phone-verification.page.html',
  styleUrls: ['./phone-verification.page.scss'],
})
export class PhoneVerificationPage implements OnInit {
  @ViewChild(IonSlides, { static: true }) slides: IonSlides;

  private DEFAULT_TIME = 60;
  timer: number = this.DEFAULT_TIME;

  timerInterval: any;
  userNum: string = ''

  smsInfo: ISmsInfo = { verificationId: '', smsCode: '' };

  user: IUser = this.srvUser.get();

  constructor(
    private navCtrl: NavController,
    private fbAuth: FirebaseAuthentication,
    private activatRoute: ActivatedRoute,
    private srvFirebase: FirebaseService,
    private srvShared: SharedServiceService,
    private srvLoader: LoadingService,
    private srvUser: UserService,
    private diagnostic: Diagnostic,
    private srvPush: PushNotificationService,
    private cdh: ChangeDetectorRef
  ) {
    this.activatRoute.queryParams.subscribe((query) => {
      this.userNum = query.phoneNumber;
    });
  }

  validate: { isNameValidate: boolean, isEmailValidate: boolean } = { isEmailValidate: false, isNameValidate: false }

  ngOnInit() {
    this.slides.lockSwipes(true);
    this.startTimer();
    this.sendCode();
  }

  sendCode() {
    // this.checkIfUserExists(); 
    if (window.isMobileWeb) { return; }
    this.fbAuth.verifyPhoneNumber(this.userNum, 0).then((verificationId) => {
      this.smsInfo.verificationId = verificationId;
      this.srvShared.showToast("CODE_SEND_SUCCESSFULLY", { number: this.userNum });
    }).catch((err) => {
      console.log(err);
      alert(err);
    });

  }

  startTimer() {
    this.timerInterval = setInterval(() => {
      this.timer--;
      if (this.timer == 0) {
        clearInterval(this.timerInterval);
      }
    }, 1000)
  }

  inputChange(ev) {
    if (this.smsInfo.smsCode.length >= 6) {
      this.srvLoader.showLoading();
      this.verifyCode();
    }
  }

  verifyCode() {
    if (window.isMobileWeb) { this.srvLoader.hideLoading(); this.cdh.detectChanges(); this.checkIfUserExists(); return; }
    this.fbAuth.signInWithVerificationId(this.smsInfo.verificationId, this.smsInfo.smsCode).then((res) => {
      console.log("i am here after phone verified");
      this.srvLoader.hideLoading();
      this.cdh.detectChanges();
      this.checkIfUserExists();
    }).catch((err) => {
      console.log("i am here after phone err", err);
      this.srvLoader.hideLoading();
      this.srvShared.showToast(err);
      this.cdh.detectChanges();
    });
  }

  checkIfUserExists() {
    console.log("in if function");
    this.srvFirebase.checkIfRegisteredAsDriver(this.userNum).then((driverUser: IUser) => {
      driverUser.userType = 'driver';
      this.proceed(driverUser);
      this.navCtrl.navigateForward('driver-tabs');
      this.srvPush.init();
    }).catch(() => {
      console.log("in if function in reject");
      this.srvFirebase.checkIfAlreadyUserExists(this.userNum).then((fbuser: IUser) => {
        fbuser.userType = 'user';
        this.proceed(fbuser);
        this.srvPush.init();
        this.navCtrl.navigateRoot('tabs');
      }).catch(() => {
        console.log("i am goind to slide 2");
        this.gotToSlide2();
        // this.createUser();
      });
    });
  }

  proceed(fbuser: any) {
    console.log("in process");
    let user: IUser = fbuser;

    this.checkPermission();
    user.isLoggedIn = true;
    this.srvUser.set(user);
    this.srvLoader.hideLoading();
  }

  register() {
    if (!this.validateUser()) { return; }
    // if (!window.isMobileWeb) { this.srvLoader.showLoading(); this.checkIfUserExists(); return; };
    this.createUser();
  }

  createUser() {
    // this.srvLoader.showLoading();
    let user: IUser = this.srvUser.createEmptyUser();
    user.registeredDate = moment().format('llll');
    user.updatedDate = moment().format('llll');
    user.phoneNumber = this.userNum;
    user.displayName = this.user.displayName;
    user.email = this.user.email;
    user.isAgreementSigned = this.user.isAgreementSigned;
    console.log("i am here");
    this.srvFirebase.createUserAndReg(user, (resp: ILocalResp) => {
      console.log("i am here 2:  ", resp);
      switch (resp.status) {
        case "OK":
          this.srvLoader.hideLoading();
          this.srvPush.init();
          this.navCtrl.navigateRoot("tabs");
          break;

        case "ERROR":
          this.srvLoader.hideLoading();
          this.srvShared.showAlert(resp.message);
          break;

        default:
          break;
      }
    });
  }


  validateUser() {
    if (!this.user.displayName.trim().length) { this.srvShared.showToast("ENTER_USERNAME");; return false; }
    else if (!this.srvShared.ValidateEmail(this.user.email)) { this.srvShared.showToast("ENTER_EMAIL"); return false; }
    else return true;
  }

  resendCode() {
    this.sendCode();
    this.timer = this.DEFAULT_TIME;
    this.startTimer();
  }

  gotToSlide2() {
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipes(true);
  }

  goBack() {
    this.navCtrl.back();
  }

  checkPermission() {
    this.diagnostic.getLocationAuthorizationStatus().then((status) => {
      console.log("Location Permission Status: " + status);
      if (status == "not_determined" || status == "NOT_REQUESTED") {

        this.user.permissions.haslocationPermission = false;

      }
      else if (status == "denied" || status == "DENIED") {
        this.user.permissions.haslocationPermission = false;
      }
      else if (status == "authorized" || status == "authorized_when_in_use" || status == "GRANTED") {
        this.user.permissions.haslocationPermission = true;
      }


      if (window.platform == 'ios') {
        this.diagnostic.getRemoteNotificationsAuthorizationStatus().then((status) => {
          console.log("Notification Permission Status: " + status);
          if (status == "not_determined") {
            this.user.permissions.hasNotificationPermission = false;
          }
          else if (status == "denied") {
            this.user.permissions.hasNotificationPermission = false;
          }
          else if (status == "authorized") {
            this.user.permissions.hasNotificationPermission = true;

          } else if (status == "denied_always") {
            this.user.permissions.hasNotificationPermission = false;
          }

        })
      }
    }).catch(() => { });
  }

  changeNameEve($event) {
    console.log("i am changing name", this.srvShared.validateName(this.user.displayName));
    if (this.user.displayName.length > 0) {
      if (!this.srvShared.validateName(this.user.displayName)) {
        this.validate.isNameValidate = true;
      } else this.validate.isNameValidate = false;
    }
  }

  changeemailEve($event) {
    console.log("i am changing email", this.srvShared.ValidateEmail(this.user.email));
    if (this.user.email.length > 0) {
      if (!this.srvShared.ValidateEmail(this.user.email)) {
        this.validate.isEmailValidate = true;
      } else this.validate.isEmailValidate = false;
    }
  }



}
