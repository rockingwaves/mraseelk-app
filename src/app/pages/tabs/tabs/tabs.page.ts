import { Component, ViewChild, OnInit } from '@angular/core';
import { IonTabs } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';
import { PushNotificationService } from 'src/app/services/push-notification';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {
  @ViewChild(IonTabs, { static: false }) tabs: IonTabs;

  selectedTab: string = 'tab1';
  user: IUser = this.srvUser.get();
  constructor(
    private srvUser: UserService,
    private srvPush: PushNotificationService
  ) { }

  ngOnInit() {
    if (!this.user.permissions.hasNotificationPermission) {
      this.srvPush.init();
    }
  }

  tabChanged(ev) {
    this.selectedTab = this.tabs.getSelected();
  }

}
