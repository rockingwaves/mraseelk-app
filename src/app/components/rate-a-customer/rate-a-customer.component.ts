import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { LoadingService } from 'src/app/services/loading.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-rate-a-customer',
  templateUrl: './rate-a-customer.component.html',
  styleUrls: ['./rate-a-customer.component.scss'],
})
export class RateACustomerComponent implements OnInit {
  rating: number = 0;
  user: IUser = this.srvUser.get();
  driver: IDriverOffer;
  @Input() requestData: IOrder;
  totalRating = 5;
  @Input() maidId: number;
  @Output() onAction: EventEmitter<boolean> = new EventEmitter();
  type: string = this.user.userType;
  constructor(
    private srvUser: UserService, private srvLoader: LoadingService,
    private srvFirebase: FirebaseService
  ) { }

  ngOnInit() {
    console.log("requestData", this.requestData);
    this.driver = this.requestData.driverOffer;
    console.log("this.driver", this.driver);
  }

  rate(rate: number) {
    this.rating = rate;
    console.log(this.rating)
  }


  submit() {
    this.srvLoader.showLoading();
    if (this.type == 'user') {
      let maidRating = (((parseInt(this.driver.rating ? this.driver.rating : "0") * 5) + 5) + this.rating) / 6;
      this.srvFirebase.updateDriver(this.driver.phoneNumber, {
        'rating': maidRating.toFixed(1).toString()
      }).then(() => {
        firebase.firestore().collection("orders").doc(this.requestData.orderId).update({ isCustomerRated: true });
        this.onAction.emit(true);
        this.srvLoader.hideLoading();
      }).catch(err => {
        this.onAction.emit(true);
        this.srvLoader.hideLoading();
      });
    } else {
      this.srvFirebase.getUserByPhoneNumber(this.requestData.userId, "users", (resp: ILocalResp) => {
        switch (resp.status) {
          case "OK":
            let _user: IUser = resp.message;
            let maidRating = (((parseInt(_user.rating ? _user.rating : "0") * 5) + 5) + this.rating) / 6;
            this.srvFirebase.updatUserInfo({ phoneNumber: this.requestData.userId }, {
              'rating': maidRating.toFixed(1).toString()
            }).then(() => {
              firebase.firestore().collection("orders").doc(this.requestData.orderId).update({ isDriverRated: true });
              this.onAction.emit(true);
              this.srvLoader.hideLoading();
            }).catch(err => {
              this.onAction.emit(true);
              this.srvLoader.hideLoading();
            });

            break;

          default:
            break;
        }

      })

    }
  }


}
