import { Component, ViewChild, OnInit } from '@angular/core';
import { IonTabs } from '@ionic/angular';
import { PushNotificationService } from 'src/app/services/push-notification';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {
  @ViewChild(IonTabs, { static: false }) tabs: IonTabs;

  selectedTab: string = 'tab1';
  constructor(
    private srvPush: PushNotificationService
  ) { }

  ngOnInit() {
    this.srvPush.init();
  }

  tabChanged(ev) {
    this.selectedTab = this.tabs.getSelected();
  }

}
