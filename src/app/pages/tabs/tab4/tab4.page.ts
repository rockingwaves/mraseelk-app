import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { ActivatedRoute, NavigationExtras } from '@angular/router';
import * as _ from 'lodash';
import { NavController } from '@ionic/angular';
import { SharedServiceService } from 'src/app/services/shared-service.service';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {

  isLoading: boolean = true;
  segmentValue: string = "history";
  historyOrders: IOrder[] = [];
  runningOrders: IOrder[] = [];
  constructor(
    private srvFirebase: FirebaseService,
    private activatedRoute: ActivatedRoute,
    private navCtrl: NavController,
    private srvShared: SharedServiceService
  ) { }

  ngOnInit() {
    console.log("i am here get orders");
    this.getOrders();
    this.activatedRoute.queryParams.subscribe((params) => {
      if (params && params['isFromCart']) {
        console.log("i am from cart");
        this.segmentValue = "sechduled";
      }
    })
  }

  ionViewWillEnter() {
    this.getOrders();
  }

  subEvents() {
    this.srvShared.$orderAdded.subscribe(() => {
      this.getOrders();
    })
  }


  getOrders() {
    this.srvFirebase.getMyRunningOrders((resp: ILocalResp) => {
      console.log(resp);
      switch (resp.status) {
        case "OK":
          this.historyOrders = _.filter(resp.message, (order: IShoppingCart) => { return order.status == 'completed' }).reverse(); //_.filter(resp.message, (order: IShoppingCart) => { return order.status == 'completed' || order.isCancelledByAdmin || order.isCancelledByUser }).reverse();
          this.runningOrders = _.filter(resp.message, (order: IShoppingCart) => { return (order.status != 'completed') }).reverse(); //_.filter(resp.message, (order: IShoppingCart) => { return (order.status != 'completed' && !order.isCancelledByAdmin && !order.isCancelledByUser) }).reverse();
          if (this.runningOrders.length) this.segmentValue = 'sechduled';
          break;

        case 'ERROR':
          break;

        default:
          break;
      }
      this.isLoading = false;
    });
  }

  segmentChanged(ev: CustomEvent) {
    this.segmentValue = ev.detail.value;
  }

  goToDetail(order) {
    console.log("i am here");
    let params: NavigationExtras = {
      queryParams: {
        order: JSON.stringify(order)
      }
    }
    this.navCtrl.navigateForward('product-order-detail', params);
  }


}
