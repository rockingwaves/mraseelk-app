import { Component, OnInit, ViewChild, ElementRef, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { LocationService } from 'src/app/services/location.service';
import { Geoposition } from '@ionic-native/geolocation/ngx';
import { OrdersPage } from 'src/app/driver/modals/orders/orders.page';
import { ModalController, IonRouterOutlet, NavController, GestureController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { FirebaseService } from 'src/app/services/firebase.service';
import { UserService } from 'src/app/services/user.service';
import { ToastService } from 'src/app/services/toast.service';
import { TranslateService } from '@ngx-translate/core';
import { LoadingService } from 'src/app/services/loading.service';
import { PushNotificationService } from 'src/app/services/push-notification';
import { UserDetailDuringOrderPage } from 'src/app/modals/user-detail-during-order/user-detail-during-order.page';
import { AlertService } from 'src/app/services/alert.service';
import * as geolib from 'geolib';
import { Subscription } from 'rxjs';
declare var google;

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnDestroy {
  @ViewChild('map', { static: true }) elMap: ElementRef;
  @ViewChild('swipeDiv', { static: true }) swipeDiv: ElementRef;
  location: IGeoLocation = null;
  map: any = null;

  isShowFull: boolean = false;
  _modal: any = null;
  poly: any;
  order: IOrder = null;
  bounds: any;

  isTracking: boolean = false;

  private user: IUser = this.srvUser.get();
  driverStatus: string = 'SEARCHING_FOR_DRIVER';
  $listen: any;

  isHideSlide: boolean = false;
  marker: any[] = [];

  startUpmarker: any;
  endupMarker: any;

  showRatingPopup: boolean = false;

  trackingLatLng: ILatLng = null;
  watchingPosition: Subscription;

  constructor(
    private srvLocation: LocationService,
    private modal: ModalController,
    private routerOutlet: IonRouterOutlet,
    private activatedRoute: ActivatedRoute,
    private navCtrl: NavController,
    private srvFirebase: FirebaseService,
    private srvUser: UserService,
    private srvToast: ToastService,
    private translate: TranslateService,
    private srvLoader: LoadingService,
    private srvPush: PushNotificationService,
    private gestureCtrl: GestureController,
    private srvAlert: AlertService,
    private chd: ChangeDetectorRef
  ) {
    this.activatedRoute.queryParams.subscribe((params) => {
      console.log("params", params)
      this.order = params.order;
      this.showRatingPopup = this.order.status == 'completed' && (this.user.userType == 'user' ? this.order.isCustomerRated == false : this.order.isDriverRated == false);
      console.log("showRatingPopup 1", this.showRatingPopup);
      this.showRatingPopup == undefined ? this.showRatingPopup = true : this.showRatingPopup = this.showRatingPopup;
      console.log("showRatingPopup 2", this.showRatingPopup);
      this.location = this.order.pickupLoc.latlng;
      this.loadMap();
      this.isTracking = params.isTracking;
      // if (this.isTracking)
      this.listenRealTimeTracking();
    });

  }

  ngOnDestroy() {
    this.map = null;
    this.bounds = null;
    if (this.poly) {
      this.poly.setMap(null);
      this.poly = null;
    }
  }

  ionViewWillLeave() {
    this.map = null;
    this.bounds = null;
    if (this.poly) {
      this.poly.setMap(null);
      this.poly = null;
    }
  }



  loadMap() {
    let options = {
      center: { lat: this.location.latitude, lng: this.location.longitude },
      disableDefaultUI: true,
      mapTypeControl: false,
      draggable: true,
      clickableIcons: false,
      zoom: 16,
    };
    this.map = new google.maps.Map(this.elMap.nativeElement, options);
    if (!this.isTracking) {
      this.setmarkerOnPickup();
      if (this.order.status == 'accepted') {
        this.srvLocation.getCurrentLocation().then((loc) => {
          console.log("locss:", loc);

          this.calcRoute(this.order.pickupLoc.latlng, loc.coords, null);
          this.setmyMarker(loc.coords);
        });
      } else if (this.order.status == 'started') {
        this.startTracking();
      } else {
        this.setmarkerOnDropOff();
        this.calcRoute(this.order.pickupLoc.latlng, this.order.dropoffLoc.latlng, null);
      }
    } else if (this.order.status == 'accepted' && this.isTracking) {
      this.srvLocation.getCurrentLocation().then((loc) => {
        this.calcRoute(loc.coords, this.order.driverOffer.driverLoc.latlng, null);
        this.setmyMarker(loc.coords);
        this.setmyMarker(loc.coords, true);
      });
    } else {
      this.setmarkerOnPickup();
      this.setmarkerOnDropOff();
      this.calcRoute(this.order.pickupLoc.latlng, this.order.dropoffLoc.latlng, null);
    }


    this.bounds = new google.maps.LatLngBounds();
    this.bounds.extend({ lat: this.location.latitude, lng: this.location.longitude });
    this.bounds.extend({ lat: this.order.dropoffLoc.latlng.latitude, lng: this.order.dropoffLoc.latlng.longitude })
    this.map.fitBounds(this.bounds);
    this.chd.detectChanges();
  }


  setmarkerOnPickup() {
    this.startUpmarker = new google.maps.Marker({
      position: { lat: this.order.pickupLoc.latlng.latitude, lng: this.order.pickupLoc.latlng.longitude },
      map: this.map,
      title: 'Hello World!'
    });
  }


  setmyMarker(loc: ILatLng, isDriverMarker?: boolean) {
    this.endupMarker = new google.maps.Marker({
      position: { lat: loc.latitude, lng: loc.longitude },
      map: this.map,
      title: 'Hello World!',
      icon: isDriverMarker ? `http://maps.google.com/mapfiles/ms/icons/yellow-dot.png` : `http://maps.google.com/mapfiles/ms/icons/green-dot.png`
    });
  }

  setmarkerOnDropOff() {
    this.endupMarker = new google.maps.Marker({
      position: { lat: this.order.dropoffLoc.latlng.latitude, lng: this.order.dropoffLoc.latlng.longitude },
      map: this.map,
      title: 'Hello World!',
      icon: `http://maps.google.com/mapfiles/ms/icons/green-dot.png`
    });
  }

  calcRoute(point1: ILatLng, point2: ILatLng, callback: () => void) {

    var path = new google.maps.MVCArray();

    //Initialize the Direction Service
    var service = new google.maps.DirectionsService();
    //Set the Path Stroke Color
    this.poly = new google.maps.Polyline({ map: this.map, strokeColor: '#ff8c00', strokeWeight: 7 });
    let start = new google.maps.LatLng(point1.latitude, point1.longitude);
    let end = new google.maps.LatLng(point2.latitude, point2.longitude);
    var src = start;
    var des = end;
    path.push(src);
    this.poly.setPath(path);
    service.route({
      origin: src,
      destination: des,
      travelMode: google.maps.DirectionsTravelMode.DRIVING
    }, function (result, status) {
      if (status == google.maps.DirectionsStatus.OK) {
        for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
          path.push(result.routes[0].overview_path[i]);
        }
        setTimeout(() => {
          callback && callback();
        }, 300);
      }
    });
  }

  close() {
    this.navCtrl.pop();
  }



  listenRealTimeTracking() {
    this.$listen = this.srvFirebase.driverRealtimeEvents(this.order.orderId, (resp: ILocalResp) => {
      switch (resp.status) {
        case "OK":
          const _order: IOrder = resp.message;
          this.order = resp.message;
          if (this.user.userType == 'user') this.handleOrderStates();

          if (this.order.orderPriceStatus != 'accepted'
            && this.user.userType == 'user'
            && this.order.orderPriceOffer.price
            && this.order.status == 'accepted') {
            this.showOfferPopup();
          }

          if (this.order.orderPriceStatus == 'request_to_next_driver' && this.user.userType == 'driver' && this.order.status == 'pending') {
            this.navCtrl.pop();
            this.srvAlert.show("User has cancelled the order!");
            this.updateOrderStatus({ orderPriceStatus: 'requested' })
          }
          if (this.isTracking && this.order.status == 'accepted')
            this.srvLocation.getCurrentLocation().then((loc) => {
              this.removeMarkers(() => {
                this.calcRoute(loc.coords, _order.driverOffer.driverLoc.latlng, () => {
                  this.startUpmarker = new google.maps.Marker({
                    position: { lat: loc.coords.latitude, lng: loc.coords.longitude },
                    map: this.map,
                    title: 'Hello World!'
                  });
                  this.endupMarker = new google.maps.Marker({
                    position: { lat: _order.driverOffer.driverLoc.latlng.latitude, lng: _order.driverOffer.driverLoc.latlng.longitude },
                    map: this.map,
                    title: 'Hello World!',
                    icon: 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png'
                  });
                  // this.setmyMarker(loc.coords);
                  // this.setmyMarker(_order.driverOffer.driverLoc.latlng, true);
                  this.map.setCenter(new google.maps.LatLng(loc.coords.latitude, loc.coords.longitude));
                  this.bounds = null;
                  this.bounds = new google.maps.LatLngBounds();
                  this.bounds.extend({ lat: loc.coords.latitude, lng: loc.coords.longitude });
                  this.bounds.extend({ lat: this.order.pickupLoc.latlng.latitude, lng: this.order.pickupLoc.latlng.longitude })
                  this.map.fitBounds(this.bounds);
                });
              });
            });
          break;

        default:
          break;
      }
    });
  }

  showOfferPopup() {
    this.srvAlert.confirm({
      message:
        '<div>' +
        `<p class="ion-no-margin w700 font-11">SDG${this.order.orderPriceOffer.price}</p>` +
        `<p >${this.order.orderPriceOffer.descp}</p>` +
        '</div>'
      , header: this.translate.instant("ORDER_OFFER_HEADER"),
      cancelBtnText: this.translate.instant("CANCEL_ORDER"),
      confirmBtnText: this.translate.instant("ACCEPT_OFFER")
    }).then(() => {
      this.srvLoader.showLoading();
      this.updateOrderStatus({ orderPriceStatus: 'accepted' })
    }).catch(() => {
      this.userCancelOrder();
    });
  }

  userCancelOrder() {
    this.srvLoader.showLoading();
    let orderAction = {
      driverOffer: null,
      status: 'pending',
      orderPriceOffer: { price: '', descp: '' },
      orderPriceStatus: 'request_to_next_driver'
    }
    this.updateOrderStatus(orderAction);
    this.srvPush.sendPush({
      token: this.order.driverOffer.deviceToken,
      bodyText: this.user.displayName + ' ' + this.translate.instant("HAS_CANCELLED_YOUR_ORDER"),
      title: this.translate.instant("MRASEELK"),
      type: 'order_user_cancelled'
    });
  }



  handleOrderStates() {
    switch (this.order.status) {
      case "pending":
        break;

      case "accepted":
        this.driverStatus = 'ON_HIS_WAY_TO_PICKUP_YOUR_ORDER';

        break;

      case "arrived":
        this.driverStatus = 'HAS_BEEN_ARRIVED';
        break;

      case 'started':
        this.driverStatus = 'ON_HIS_WAY_TO_COMPLETE_YOUR_ORDER';
        break;

      case 'completed':
        this.driverStatus = 'HAS_COMPLETED_THIS_ORDER';
        // this.showRatingPopup = true;
        break;

      default:
        break;
    }

  }

  OrderAccept() {
    this.srvAlert.confirm({ message: this.translate.instant("ARE_YOU_SURE_YOU_WANT_TO_ACCEPT"), confirmBtnText: this.translate.instant("YES") }).then(() => {
      this.srvLoader.showLoading();
      this.srvLocation.getCurrentLocation().then((loc) => {
        let orderAction = {
          driverOffer: {
            driverLoc: {
              latlng: loc.coords
            },
            driverName: this.user.displayName,
            phoneNumber: this.user.phoneNumber,
          },
          status: 'accepted'
        }
        this.order.status = 'accepted';
        this.updateOrderStatus(orderAction);
        this.resetPath(loc);
        this.srvPush.sendPush({
          token: this.order.userDeviceToken,
          bodyText: this.user.displayName + ' ' + this.translate.instant("ON_HIS_WAY_TO_PICKUP_YOUR_ORDER"),
          title: this.translate.instant("ORDER_HAS_BEEN_ACCEPTED"),
          type: 'order_accepted'
        })
      }).catch(() => { });
    }).catch(() => { })
  }

  resetPath(loc) {
    if (this.poly) {
      this.poly.setMap(null);
      this.poly = null;
    }
    // this.srvLocation.getCurrentLocation().then((loc) => {
    if (window.isMobileWeb) loc.coords.latitude = 33.574924; loc.coords.longitude = 73.133402;
    this.removeMarkers(() => {
      this.calcRoute(loc.coords, this.order.pickupLoc.latlng, () => {
        this.startUpmarker = new google.maps.Marker({
          position: { lat: loc.coords.latitude, lng: loc.coords.longitude },
          map: this.map,
          title: 'Hello World!'
        });
        this.endupMarker = new google.maps.Marker({
          position: { lat: this.order.pickupLoc.latlng.latitude, lng: this.order.pickupLoc.latlng.longitude },
          map: this.map,
          title: 'Hello World!',
          icon: 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png'
        });
        this.map.setCenter(new google.maps.LatLng(loc.coords.latitude, loc.coords.longitude));
        this.bounds = null;
        this.bounds = new google.maps.LatLngBounds();
        this.bounds.extend({ lat: loc.coords.latitude, lng: loc.coords.longitude });
        this.bounds.extend({ lat: this.order.pickupLoc.latlng.latitude, lng: this.order.pickupLoc.latlng.longitude })
        this.map.fitBounds(this.bounds);
      });
    });
    // });
  }

  setMarkers() {
    for (let i = 0; i < this.marker.length; i++) {
      this.marker[i].setMap(this.map);
    }
  }

  removeMarkers(callback: () => void) {
    this.startUpmarker && this.startUpmarker.setMap(null);
    this.endupMarker && this.endupMarker.setMap(null);
    setTimeout(() => {
      callback && callback();

    }, 300);
    // for (let i = 0; i < this.marker.length; i++) {
    //   this.marker[i].setMap(null);
    // }
  }


  updateOrderStatus(orderAction) {
    this.srvFirebase.orderAction(this.order.orderId, orderAction, (resp: ILocalResp) => {
      switch (resp.status) {
        case "OK":
          // this.srvToast.show({ message: this.translate.instant("STATUS_") });
          // this.srvPush.sendPush({
          //   token: this.order.userDeviceToken,
          //   bodyText: this.user.displayName + ' ' + this.translate.instant("SENT_YOU_INFORMATION_ABOUT_PRODUCT"),
          //   title: this.translate.instant("ORDER_HAS_BEEN_ACCEPTED"),
          //   type: 'order_accepted'
          // })
          this.srvLoader.hideLoading();
          break;

        case "ERROR":
          this.srvToast.show({ message: resp.message });
          this.srvLoader.hideLoading();
          break;
        default:
          break;
      }

    });
  }

  arrived() {
    this.srvLoader.showLoading();
    this.order.status = 'arrived';
    this.updateOrderStatus({ status: 'arrived' });
    this.resetPath({ coords: this.order.dropoffLoc.latlng });
    this.srvPush.sendPush({
      token: this.order.userDeviceToken,
      bodyText: this.user.displayName + ' ' + this.translate.instant("HAS_ARRIVED_TO_YOUR_LOCATION"),
      title: this.translate.instant("DRIVER_HAS_ARRIVED"),
      type: 'order_driver_arrived'
    });
  }

  start() {
    this.srvLoader.showLoading();
    this.order.status = 'started';
    this.updateOrderStatus({ status: 'started' });
    this.startTracking();
    this.srvPush.sendPush({
      token: this.order.userDeviceToken,
      bodyText: this.user.displayName + ' ' + this.translate.instant("ON_HIS_WAY_TO_COMPLETE_YOUR_ORDER"),
      title: this.translate.instant("MRASEELK"),
      type: 'order_driver_started'
    });
  }

  startTracking() {
    let distance = 0;
    this.watchingPosition = this.srvLocation.watchPosition().subscribe((data) => {
      if (this.trackingLatLng) {
        distance = geolib.getDistance(data.coords, this.trackingLatLng);
      }
      const { coords } = data;
      if (distance == 0 || distance > 100) {
        this.srvFirebase.updatDriverInfo(this.user, {
          driverOffer: {
            driverLoc: {
              latlng: {
                latitude: coords.latitude,
                longitude: coords.longitude
              }
            }
          }
        })
        this.resetPath({ coords: data });
      }
    });
  }

  complete() {
    this.srvLoader.showLoading();
    this.order.status = 'completed';
    this.updateOrderStatus({ status: 'completed' });
    this.srvFirebase.deleteConversation(this.order.orderId, () => { });
    this.watchingPosition.unsubscribe();
    this.srvPush.sendPush({
      token: this.order.userDeviceToken,
      bodyText: this.user.displayName + ' ' + this.translate.instant("HAS_SAFELY_DELIVER_YOUR_ORDER"),
      title: this.translate.instant("MRASEELK"),
      type: 'order_completed'
    });
    this.showRatingPopup = true;
  }

  OrderCancellation() {
    this.srvLoader.showLoading();
    let orderAction = {
      driverOffer: null,
      status: 'pending'
    }
    this.updateOrderStatus(orderAction);
    this.srvPush.sendPush({
      token: this.order.userDeviceToken,
      bodyText: this.user.displayName + ' ' + this.translate.instant("HAS_CANCELLED_YOUR_ORDER"),
      title: this.translate.instant("MRASEELK"),
      type: 'order_driver_cancelled'
    });
  }


  async goToDetails() {
    let modal = await this.modal.create({
      component: UserDetailDuringOrderPage,
      presentingElement: this.routerOutlet.nativeEl,
      swipeToClose: true,

      componentProps: {
        userId: this.user.userType == 'driver' ? this.order.userId : this.order.driverOffer.phoneNumber,
        type: this.user.userType == 'driver' ? 'users' : 'drivers',
        orderId: this.order.orderId,
      }
    });
    modal.present();
  }

  hideShowDiv() {
    this.isHideSlide = !this.isHideSlide;
  }


  ratingDone() {
    this.user.userType == 'user' ? this.order.isCustomerRated = true : this.order.isDriverRated = true;
  }

  submit() {
    // console.log("called")
    this.user.userType == 'user' ? this.order.isCustomerRated = true : this.order.isDriverRated = true;
    // this.showRatingPopup = this.order.status == 'completed' && (this.user.userType == 'user' ? !this.order.isCustomerRated : !this.order.isDriverRated);
    // console.log("this.showRatingPopup", this.showRatingPopup);
    this.showRatingPopup = false;
    this.user.userType == 'user' ? this.navCtrl.navigateBack('tabs') : this.navCtrl.pop();
  }

  async sendOffer() {
    const alert = await this.srvAlert.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Prompt!',
      message: this.translate.instant("LET_CUSTOMER_NOW_THE_PRODUCT_PRICE_AND_DETAILS"),
      inputs: [
        {
          name: 'price',
          type: 'tel',
          placeholder: 'Enter Price'
        },
        {
          name: 'paragraph',
          id: 'paragraph',
          type: 'textarea',
          placeholder: 'such as expiry date etc.'
        },

      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (res: { paragraph: string, price: string }) => {
            this.srvLoader.showLoading();
            this.order.orderPriceOffer = {
              descp: res.paragraph,
              price: res.price
            }
            this.updateOrderStatus({ orderPriceOffer: this.order.orderPriceOffer });
            this.listenRealTimeTracking();
          }
        }
      ]
    });

    await alert.present();
  }

}
