import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute, NavigationExtras } from '@angular/router';
import { SharedServiceService } from 'src/app/services/shared-service.service';
import { Constant } from 'src/app/services/constants.service';
import { LocationService } from 'src/app/services/location.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { LoadingService } from 'src/app/services/loading.service';
import { AlertService } from 'src/app/services/alert.service';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from 'src/app/services/user.service';
import * as _ from 'lodash';


@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.page.html',
  styleUrls: ['./shopping-cart.page.scss'],
})
export class ShoppingCartPage implements OnInit {
  product: IProducts = null;
  deliveryCharges: number = 0;
  quantity: number = 1;
  shoppingCart: IShoppingCart;

  constructor(
    private navCtrl: NavController,
    private srvLoc: LocationService,
    private srvFirebase: FirebaseService,
    private srvLoader: LoadingService,
    private srvAlert: AlertService,
    private translate: TranslateService,
    private userService: UserService
  ) {
  }

  ngOnInit() {
    this.shoppingCart = this.userService.get().shoppingCart || this.shoppingCart;
    console.log("this.shoppingCart: ", JSON.stringify(this.shoppingCart));
    if (this.shoppingCart && this.shoppingCart.products && this.shoppingCart.products.length) {
      this.getCurrentLoc();
    }
  }

  goBack() {
    this.navCtrl.pop();
  }

  getCurrentLoc() {
    this.srvLoc.getCurrentLocation().then((resp) => {
      this.shoppingCart.deliveryLatlng = resp.coords;
      let distance = this.srvLoc.distance(Constant.appValues.storeLatitude, Constant.appValues.storeLongitude, resp.coords.latitude, resp.coords.longitude, 'K').toFixed(0);
      console.log("distance: ", distance);
      this.deliveryCharges = parseFloat(distance) > 5 ? (Constant.appValues.till5KM + ((parseFloat(distance) - 5) * Constant.appValues.perKM)) : Constant.appValues.till5KM;
      this.calculateTotalAmount();
      // console.log('delivery charges: ', this.deliveryCharges);
    });
  }

  Order() {
    this.srvAlert.confirm({
      message:
        this.translate.instant("ARE_YOU_SURE_YOU_WANT_TO_PROCEED")
    }).then(() => {
      this.srvLoader.showLoading();
      this.shoppingCart.deliveryFee = this.deliveryCharges;
      this.shoppingCart.status = 'pending';
      this.srvFirebase.addProductOrder(this.shoppingCart, (resp: ILocalResp) => {
        switch (resp.status) {
          case "OK":
            this.userService.set({
              shoppingCart: { products: [] }
            }).then(() => {
              this.srvAlert.show(this.translate.instant("YOUR_ORDER_HAS_BEEN_PLACED"), this.translate.instant('THANK_YOU'));
              let params: NavigationExtras = {
                queryParams: {
                  isFromCart: true
                }
              }
              this.navCtrl.navigateForward(['tabs/tabs/tab4'], params);
              this.srvLoader.hideLoading();
            })
            break;

          case "ERROR":
            this.srvLoader.hideLoading();
            this.srvAlert.show(resp.message);
            break;

          default:
            break;
        }
      })
    })

  }

  removeProduct(index: number) {
    this.shoppingCart.products.splice(index, 1);
    this.userService.set({
      shoppingCart: this.shoppingCart
    });
    this.calculateTotalAmount();
  }

  incrQty(product: IProductsOrder) {
    if (product.quantity >= 5) { return }
    product.quantity += 1;
    this.calculateTotalAmount();
  }

  decrQty(product: IProductsOrder) {
    if (product.quantity == 1) { return }
    product.quantity -= 1;
    this.calculateTotalAmount();
  }


  calculateTotalAmount() {
    let totalAmount = 0;
    this.shoppingCart.products.forEach((product: IProductsOrder) => {
      totalAmount += (product.productPrice * product.quantity);
    })
    console.log("total amount: ", totalAmount);
    this.shoppingCart.totalAmount = this.deliveryCharges + totalAmount;
  }


}
