import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { AlertService } from 'src/app/services/alert.service';
import { LoadingService } from 'src/app/services/loading.service';
import { NavController } from '@ionic/angular';
import { SharedServiceService } from 'src/app/services/shared-service.service';

@Component({
  selector: 'app-product-order-detail',
  templateUrl: './product-order-detail.page.html',
  styleUrls: ['./product-order-detail.page.scss', './../shopping-cart/shopping-cart.page.scss'],
})
export class ProductOrderDetailPage implements OnInit {
  user: IUser;
  order: IShoppingCart;
  constructor(
    private firebaseService: FirebaseService,
    private activeRouter: ActivatedRoute,
    private navCtrl: NavController,
    private alertService: AlertService,
    private srvShared: SharedServiceService
  ) { }

  ngOnInit() {
    this.activeRouter.queryParams.subscribe((params) => {
      this.order = JSON.parse(params['order']);
      // console.log("order: ", JSON.stringify(this.order));
    })
  }

  cancelOrder() {
    this.alertService.confirm({ message: "Are you sure you want to cancel this order?" }).then((isConfirmed) => {
      if (isConfirmed) {
        this.firebaseService.updateProductOrder(this.order.orderId, {
          isCancelledByUser: true
        });
        this.order.isCancelledByUser = true;
        this.srvShared.$getAllProductOrders.emit(true);
      }
    })
  }

  goBack() {
    this.navCtrl.pop();
  }

}
