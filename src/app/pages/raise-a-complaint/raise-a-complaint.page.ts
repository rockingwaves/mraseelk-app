import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import * as moment from 'moment';
import { FirebaseService } from 'src/app/services/firebase.service';
import { SharedServiceService } from 'src/app/services/shared-service.service';
import { LoadingService } from 'src/app/services/loading.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-raise-a-complaint',
  templateUrl: './raise-a-complaint.page.html',
  styleUrls: ['./raise-a-complaint.page.scss'],
})
export class RaiseAComplaintPage implements OnInit {
  user: IUser = this.srvUser.get();

  info: IComplain = {
    dateAdded: '',
    id: '',
    message: '',
    status: 'pending',
    userPhone: this.user.phoneNumber

  }
  constructor(
    private srvUser: UserService,
    private srvFirebase: FirebaseService,
    private srvShared: SharedServiceService,
    private srvLoader: LoadingService,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  onSend() {
    if (!this.info.message.length) { this.srvShared.showToast("COMPLAINT_BOX_CANT_BE_EMPTY"); return; }
    else {
      this.srvLoader.showLoading();
      this.info.dateAdded = moment().format('llll');
      this.info.id = 'id-' + new Date().getTime();
      this.srvFirebase.sendComplain(this.info, (resp: ILocalResp) => {
        switch (resp.status) {
          case "OK":
            this.srvLoader.hideLoading();
            this.srvShared.showToast("COMPLAINT_REGISTERED_SUCCESSFULLY");
            this.navCtrl.pop();
            break;

          case "ERROR":
            this.srvLoader.hideLoading();
            this.srvShared.showAlert(resp.message);
            break;

          default:
            break;
        }
      });
    }
  }

}
