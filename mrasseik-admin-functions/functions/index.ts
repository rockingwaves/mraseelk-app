const functions = require('firebase-functions');
const cors = require('cors')({ origin: true });
const admin = require('firebase-admin');
admin.initializeApp();

const nodemailer = require('nodemailer');
const fromEmail = 'mraseelkapp@gmail.com';
let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    service: 'gmail',
    auth: {
        user: fromEmail,
        pass: 'Mra@#123456'
    }
});

exports.sendMail = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        console.log("dest i am here");
        let dest = JSON.parse(req.query.dest);
        console.log("dest: " , dest);
        const mailOptions = {
            from: fromEmail,
            to: fromEmail,
            cc:'zeeshan.aslam2018@gmail.com',
            subject: 'New Order Requrest Received',
            html: `
            <p>Hi,</p>
            <p>A new order received at Mraseelk store. Click on below link to view details</p>
            <a href="https://mraseelk-5f2ec.web.app/order-detail?orderId=${dest['productId']}">View Detail on Admin Dashboard</a>
            <p>Regards,</p>
            <p>Mraseelk Team</p>
            `
        };
        return transporter.sendMail(mailOptions, (erro, info) => {
            if (erro) {
                return res.send(JSON.stringify(erro.toString()));
            }
            return res.send(JSON.stringify('Sended'));
        });
    });
});


exports.sendReply = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        console.log("dest i am here");
        let dest = JSON.parse(req.query.dest);
        console.log("dest: " , dest);
        const mailOptions = {
            from: fromEmail,
            to: dest['userEmail'],
            subject: 'Reply to your Complain',
            html: `
            <p>Hi,</p>
            <p>${dest['replyMessage']}</p>
            <br>
            <p>Regards,</p>
            <p>Mraseelk Team</p>
            `
        };
        return transporter.sendMail(mailOptions, (erro, info) => {
            if (erro) {
                return res.send(JSON.stringify(erro.toString()));
            }
            return res.send(JSON.stringify('Sended'));
        });
    });
});