import { Component } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  historyOrders: IOrder[] = [];
  totalAmount: number = 0;
  constructor(
    private srvFirebase: FirebaseService,


  ) {
    this.getMyHistoryWork();
  }

  getMyHistoryWork() {
    this.srvFirebase.getMyHistoryWork((resp) => {
      switch (resp.status) {
        case "OK":
          this.historyOrders = resp.message;
          this.totalAmount = _.sumBy(this.historyOrders, (tO: IOrder) => { return tO.totalFares });
          console.log(this.totalAmount)
          break;

        default:
          break;
      }
    });
  }





}
