export class User {
    name: string;
    email: string;
    password: string;
    userId: any;
    isRegistered: boolean;
    isLoggedIn: boolean;
    registeredDate: any;
    updatedDate: any;
    platform: string;
    phoneNumber: string;
    isPhoneNumberVerified?: boolean;
    location: ILatLng;
    deviceToken: string;
    userType: string;
    allowContact?: boolean;
    lastReqAt?: any;
    latitude?: number;
    longitude?: number;
    isAgreeToTerms?: boolean;
    region?: string;
    city?: string;
    photoUrl?: string;
    displayName?: string;
    permissions: IPermissions;
    shoppingCart?: IShoppingCart;
    rating?: string;
    constructor(fields: any) {
        for (let f in fields) {
            this[f] = fields[f];
        }
    }
}