import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from './services/user.service';
import { FirebaseService } from './services/firebase.service';
import { ScriptInjectorService } from './services/script-injector.service';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import * as moment from 'moment';
import { SharedServiceService } from './services/shared-service.service';
import { CommonService } from './services/common.service';
import { Constant } from './services/constants.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  private user: IUser;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private translate: TranslateService,
    private srvUser: UserService,
    private srvFirebase: FirebaseService,
    private navCtrl: NavController,
    private srvInjector: ScriptInjectorService,
    private diagnostic: Diagnostic,
    private srvShared: CommonService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      window.moment = moment;
      window.isMobileWeb = this.platform.is('mobileweb');
      this.srvInjector.loadScript("google").then((resp) => {
        console.log("google loaded", resp);
      });
      window.isMobileWeb = this.platform.is('mobileweb');
      window.platform = this.platform.is('ios') ? 'ios' : 'android';
      this.translate.setDefaultLang('en');
      this.setStatusBar();
      this.srvUser.init().then((user: IUser) => {
        this.user = user;
        this.user.permissions = { hasNotificationPermission: false, haslocationPermission: false }
        this.checkPermission(() => {
          this.navigateUser();
        });
      });
      this.srvFirebase.initFirebase();
      this.splashScreen.hide();
    });
  }

  setStatusBar() {
    switch (window.platform) {
      case 'ios':
        this.statusBar.styleDefault();
        break;

      case 'android':
        this.statusBar.backgroundColorByHexString("#ffffff");
        this.statusBar.styleDefault();
        break;

      default:
        break;
    }
  }

  navigateUser() {
    let user = this.srvUser.get();
    // this.navCtrl.navigateRoot("phone-verification");
    if (user.isLoggedIn) {
      if (user.userType == 'driver') {
        this.navCtrl.navigateRoot('driver-tabs'); //driver-tabs
      }
      else {
        this.getAppValues()
        this.navCtrl.navigateRoot('tabs');
      }
    }
    else this.navCtrl.navigateRoot("phone-auth");
  }


  checkPermission(callback: Function) {
    this.diagnostic.getLocationAuthorizationStatus().then((status) => {
      console.log("Location Permission Status: " + status);
      if (status == "not_determined" || status == "NOT_REQUESTED") {

        this.user.permissions.haslocationPermission = false;

      }
      else if (status == "denied" || status == "DENIED") {
        this.user.permissions.haslocationPermission = false;
      }
      else if (status == "authorized" || status == "authorized_when_in_use" || status == "GRANTED") {
        this.user.permissions.haslocationPermission = true;
      }


      if (window.platform == 'ios') {
        this.diagnostic.getRemoteNotificationsAuthorizationStatus().then((status) => {
          console.log("Notification Permission Status: " + status);
          if (status == "not_determined") {
            this.user.permissions.hasNotificationPermission = false;
          }
          else if (status == "denied") {
            this.user.permissions.hasNotificationPermission = false;
          }
          else if (status == "authorized") {
            this.user.permissions.hasNotificationPermission = true;

          } else if (status == "denied_always") {
            this.user.permissions.hasNotificationPermission = false;
          }

        });
      }
      this.srvUser.set(this.user);
      callback && callback();
    }).catch(() => {
      this.user.permissions.haslocationPermission = false;
      this.user.permissions.hasNotificationPermission = false;
      this.srvUser.set(this.user);
      callback && callback();
    });
  }

  getAppValues() {
    this.srvFirebase.getAppValues((resp: ILocalResp) => {
      switch (resp.status) {
        case "OK":
          Constant.appValues = resp.message
          break;

        case "ERROR":

          break;

        default:
          break;
      }

    });
  }

}
