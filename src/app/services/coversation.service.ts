import { Injectable, EventEmitter } from '@angular/core';
import * as firebase from 'firebase';
import 'firebase/messaging';
import { UserService } from './user.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class CoversationService {
  collection: firebase.firestore.CollectionReference;
  instance: firebase.firestore.Firestore;
  public convId: any;
  public user: IUser = this.srvUser.get();
  public $typing: EventEmitter<Object> = new EventEmitter();
  private isAuthenticated = false;

  constructor(
    private srvUser: UserService,
    private http: HttpClient
  ) { }

  init() {
    if (!this.collection) {
      let _collection: firebase.firestore.CollectionReference;
      this.collection = _collection;
      this.initInstances();
    }
  }


  initInstances() {
    this.instance = firebase.firestore();
    this.collection = this.instance.collection('conversations');
  }

  getOtherUser(users: IChatUser[]) {
    return users.find(u => u.userId != this.srvUser.get().phoneNumber);
  }

  createChatID(userId, otherUserId) {
    let id = [userId, otherUserId].sort().join("_");
    return "chat_room_" + id;
  }


  createContainIds(userUd: string, otherUserId: string) {
    return [userUd, otherUserId]
  }

  timestamp() {
    return firebase.firestore.FieldValue.serverTimestamp();
  }

  initChat(chatData: IChatInit) {
    
    return new Promise((resolve, reject) => {
      console.log("chatData", chatData);
      this.collection.doc(chatData.chatId).set(Object.assign({}, chatData)).then(() => {
        resolve();
      }).catch((err) => {
        resolve(err);
      });
    });
  }

  isConvExists(convId: string, callback: (exists: boolean) => void) {

    let conversations = this.collection.where("chatId", "==", convId);
    conversations.get().then(function (querySnapshot) {
      callback(!!querySnapshot.docs.length);
    });
  }

  creator() {
    let myUser = this.srvUser.get();
    let user: IChatUser = {
      device_token: myUser.deviceToken,
      userId: myUser.phoneNumber,
      isCreator: true,
      thumb: myUser.photoUrl,
      unread_count: 0,
      username: myUser.displayName
    };
    return user;
  }

  addMessage(message: IChatMsg, convId: string) {
    // message.created = this.timestamp();
    this.collection
      .doc(convId)
      .collection('messages')
      .doc(message.id)
      .set(message);
    this.updatelastMessage(message, convId);
  }

  updatelastMessage(message, convId) {
    this.collection.doc(convId).update({
      lastMessage: message,
      updated: this.timestamp()
    });
  }

  updateUnreadCount(convId: string) {
    this.collection
      .doc(convId)
      .get()
      .then((docSnapshot) => {
        let conv = docSnapshot.data();
        conv['users'].forEach((u) => {
          if (u.userId != this.creator().userId) {
            u.unread_count = u.unread_count + 1;
          }
        });

        this.collection
          .doc(convId)
          .update({
            users: conv['users'],
          });
      });
  }

  getConversationsAndListen(callback: (conv: any) => void, callbackFinish?: () => void) {
    return this.collection
      .where('chatContainIDs', "array-contains", this.creator().userId)
      .onSnapshot((snapshot) => {
        var source = snapshot.metadata.hasPendingWrites ? "Local" : "Server";
        if (source == "Local") return;
        if (!snapshot.empty) {
          snapshot.docChanges().forEach((docChange, i) => {
            if (callback) callback(docChange.doc.data());
            if (i == snapshot.docChanges.length - 1)
              if (callbackFinish) callbackFinish();
          })
        } else {
          if (callback) callback(null);
        }
      });
  }

  getConversationMessages(convId: string) {
    return new Promise((resolve) => {
      this.collection.doc(convId)
        .collection('messages')
        .orderBy('created', "desc")
        .limit(10)
        .get()
        .then(function (querySnapshot) {
          var list = [];
          querySnapshot.forEach(function (doc) {
            list.push(doc.data());
          });
          resolve(list.reverse());
        });
    })
  }

  listen(convId: string, callback) {
    var a = 0;
    return this.collection.doc(convId)
      .collection('messages')
      .where('created', '>=', new Date())
      .orderBy('created', "desc")
      .limit(1)
      .onSnapshot((querySnapshot) => {
        var source = querySnapshot.metadata.hasPendingWrites ? "Local" : "Server";
        if (source == "Local") return;
        querySnapshot.docChanges().forEach((change) => {
          // console.log("change detected:", change.type);
          // hange.doc.data()
          if (callback) callback(change.doc.data());
        //   if (change.type === "added") {
        //     // console.log("New city: ", change.doc.data());
        //     if (callback) callback(change.doc.data());
        //   }
        //   if (change.type === "modified") {
        //     a++;
        //     console.log("aaaaaaa:", a);
        //     // console.log("isAlreadyModified:", isAlreadyModified);
        //     // if (!isAlreadyModified) {
        //     //     console.log("Modified city: ", isAlreadyModified);
        //     //     isAlreadyModified = true;
        //     if (callback) callback(change.doc.data());
        //     // }
        //   }
        //   if (change.type === "removed") {
        //     console.log("Removed city: ", change.doc.data());
        //   }
        // });
        // querySnapshot.forEach(function (doc) {
        //     if (callback) callback(doc.data());
        });
      });
  }

  clearMyUnreadCount(convId: string) {
    this.collection
      .doc(convId)
      .get()
      .then((docSnapshot) => {
        let conv: any = docSnapshot.data();
        conv.users.forEach(u => {
          if (u.userId == this.creator().userId) u.unread_count = 0;
        });

        this.collection
          .doc(convId)
          .update({
            users: conv.users
            // updated: this.timestamp()
          }).catch((error) => {
            console.log("Message Service: Clear My Count Update Error: ", error);
          });
      });
  }

  markAsDelivered(convId: string, user: IChatUser) {
    console.log("Message Service: Mark as delivered: ", convId);
    var messages = this.instance
      .collection('conversations')
      .doc(convId)
      .collection('messages');

    messages
      .where('delivered', '==', false)
      .where("user.id", "==", user.userId)
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          let msg: IChatMsg = <any>doc.data();
          msg.delivered = true;

          // msg.updated = this.timestamp();
          messages
            .doc(msg.id)
            .set(msg);
        });
      });
  }

  markAsSeen(convId: string, user: IChatUser) {
    console.log("Message Service: Mark as delivered: ", convId);
    var messages = this.instance
      .collection('conversations')
      .doc(convId)
      .collection('messages');

    messages
      .where('seen', '==', false)
      .where("user.id", "==", user.userId)
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          let msg: IChatMsg = <any>doc.data();
          msg.seen = true;

          // msg.updated = this.timestamp();
          messages
            .doc(msg.id)
            .set(msg);
        });
      });
  }

  typing(convId: string, selfUser: IChatUser, status: boolean) {
    this.collection
      .doc(this.convId)
      .update({
        typing: {
          status: status,
          user: selfUser
        }
        // updated: this.timestamp()
      })
  }

  updateMessage(convId: string, msgId: string, msg: IChatMsg) {
    if (msg["menu"]) delete msg["menu"];

    this.collection
      .doc(this.convId)
      .collection('messages')
      .doc(msgId)
      .update(msg);
  }

  messageSeen(convId: string, msgId: string) {
    this.updateMessage(convId, msgId, <any>{
      seen: true,
      delivered: true,
      created: this.timestamp(),
      updated: this.timestamp()
    });
  }

  messageDelivered(convId: string, msgId: string) {
    this.updateMessage(convId, msgId, <any>{
      delivered: true,
      created: this.timestamp(),
      updated: this.timestamp()
    });
  }

  getAllUnreadCount() {
    return new Promise((resolve: (count: number) => void) => {
      let count = 0;

      this.instance
        .collection("conversations")
        .where('chatContainIDs', "array-contains", this.srvUser.get().userId)
        .get()
        .then((querySnapshot) => {
          querySnapshot.docs.forEach((doc, i) => {
            let conv: IChatInit = <any>doc.data();
            let myChatUser = conv.users.find(u => u.userId == this.srvUser.get().userId);

            if (myChatUser) count += myChatUser.unread_count;
            if (i == querySnapshot.docs.length - 1) resolve(count);
          });
        });
    });
  }

  getMyUser(conv: IChatInit) {
    return conv.users.find(u => u.userId == this.srvUser.get().phoneNumber);
  }

  isCreatedByMe(conv: IChatInit) {
    return conv.creator.userId == this.srvUser.get().userId;
  }

  sendPush(token: string, _data: IPushData) {


    let data = {
      "to": token,
      "notification": {
        "title": _data.title,
        "body": _data.bodyText,

      },
    }

    console.log("Setting Headers");
    let headers = new HttpHeaders()
      .append('Content-Type', 'application/json')
      .append('Authorization', 'key=AAAAHhzPglo:APA91bESDwPaDLi7ssBGqCLQo1l1KuLQ4sbkO_0QMRY5_kjn9W5HI_nRUDPFZov4UjIfrScJCHtdc8FWZ2bCTQi5icLsY70ZJ60UUX4qwMkjloDkNoXs8B9M18qTsoZUEAQ4Muf3dnxY');
    this.http.post('https://fcm.googleapis.com/fcm/send', data, { headers: headers }).subscribe((res) => {
      console.log("res", res);
    }, err => { console.log("err", err); })

  }


  getUserToken(userId: string, callback: (token: string) => void) {
    firebase.firestore().collection('users').doc(userId).
      get().then((doc) => {
        if (doc.exists) {
          let user: IUser = <IUser>doc.data();
          callback && callback(user.deviceToken);
        } else {
          callback && callback('');
        }
      }).catch(() => {
        callback && callback('');
      });

  }

}
