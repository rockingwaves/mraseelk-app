import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { NavParams, IonContent, ModalController } from '@ionic/angular';
import { CoversationService } from 'src/app/services/coversation.service';
import { UserService } from 'src/app/services/user.service';
import * as moment from 'moment';

@Component({
  selector: 'app-chat-view',
  templateUrl: './chat-view.page.html',
  styleUrls: ['./chat-view.page.scss'],
})
export class ChatViewPage implements OnInit, OnDestroy {
  @ViewChild(IonContent, { static: true }) content: IonContent;

  conv: IChatInit = this.navParams.get('conv');
  private isChatExist: boolean = false;
  otherUser: IChatUser = null;
  chatMsgs: IChatMsg[] = [];
  $stopListener: any;

  textmsg: string = '';

  private user: IUser = this.srvUser.get();
  constructor(
    private navParams: NavParams,
    private srvConv: CoversationService,
    private srvUser: UserService,
    private modalCtrl: ModalController
  ) {
    console.log(this.conv);
    this.otherUser = this.srvConv.getOtherUser(this.conv.users);
  }

  ngOnInit() {
    this.fetchConv(() => {
      if (this.isChatExist) {
        this.getConvMessages();
      } else {
        this.listenRealTimeMsgs();
      }
    });
  }

  ngOnDestroy() {
    this.$stopListener && this.$stopListener();
  }


  fetchConv(callback: () => void) {
    this.srvConv.isConvExists(this.conv.chatId, (isExist: boolean) => {
      this.isChatExist = isExist;
      callback();
    });
  }

  getConvMessages() {
    this.srvConv.getConversationMessages(this.conv.chatId).then((messages: IChatMsg[]) => {
      messages.forEach(element => { this.processMessage(element) });
      console.log(messages);
      if (messages.length) this.chatMsgs = messages;
      this.listenRealTimeMsgs();
      this.scrollBottom();

      this.srvConv.clearMyUnreadCount(this.conv.chatId);
      // if (this.otherUser) {
      //   console.log("is i am called from getConvMessages?")
      //   this.srvConv.markAsDelivered(this.conv.chatId, this.otherUser);
      //   this.srvConv.markAsSeen(this.conv.chatId, this.otherUser);
      // }
    });
  }

  processMessage(msg: IChatMsg) {
    msg.isMine = msg.user.userId == this.user.phoneNumber;
    console.log();
    msg.created = moment(msg.created.toDate()).fromNow();
  }

  listenRealTimeMsgs() {
    this.$stopListener = this.srvConv.listen(this.conv.chatId, (message: IChatMsg) => {

      var doesMsgExists = this.chatMsgs.filter((m) => {
        return m.id == message.id;
      });

      console.log("doesmsgEx", doesMsgExists);
      if (!doesMsgExists.length) {
        this.srvConv.clearMyUnreadCount(this.conv.chatId);
        message.isMine = message.user.userId == this.user.phoneNumber;
        message.created = moment(message.created.toDate()).fromNow();
        this.chatMsgs.push(message);
        if (this.otherUser) {
          // console.log("is i am called from listenRealTimeMsgs?")
          this.srvConv.markAsDelivered(this.conv.chatId, this.otherUser);
          this.srvConv.markAsSeen(this.conv.chatId, this.otherUser);
        }
      } else {
        message.isMine = message.user.userId == this.user.phoneNumber;
        message.created = moment(message.created.toDate()).fromNow();
        this.chatMsgs[this.chatMsgs.indexOf(doesMsgExists[0])] = message;
      }
      this.scrollBottom();
    });
  }

  scrollBottom(callback?: Function) {
    setTimeout(() => {
      this.content.scrollToBottom(200);
      if (callback) callback();
    }, 200);
  }

  goBack() {
    this.modalCtrl.dismiss();
  }

  sendMsg() {
    if (!this.textmsg.trim().length) return;
    let message: IChatMsg = {
      content: this.textmsg,
      created: this.srvConv.timestamp(), //moment().format(),
      timeFormatted: moment().format(),
      delivered: false,
      isMine: true, seen: false,
      type: 'text',
      user: this.srvConv.creator(),
      id: new Date().getTime().toString()
    }
    this.textmsg = '';
    // message.created = moment(message.created.toDate()).fromNow();
    this.chatMsgs.push(message);

    console.log(this.conv);
    if (!this.isChatExist) this.createConversation(() => { this.updateMsgToDB(message); this.listenRealTimeMsgs(); });
    else this.updateMsgToDB(message)
    this.scrollBottom();
  }

  updateMsgToDB(msgObj: IChatMsg) {
    let msg: IChatMsg = Object.assign({}, msgObj);
    delete msg.isMine;
    console.log("msg", msg);
    this.srvConv.addMessage(msgObj, this.conv.chatId);
    this.srvConv.updateUnreadCount(this.conv.chatId);
    this.sendPush(msg);
  }

  sendPush(msg: IChatMsg) {
    this.srvConv.sendPush(this.otherUser.device_token, {
      title: this.user.displayName,
      bodyText: msg.content,
      token: this.otherUser.device_token,
      type: 'text-message'
    })
  }

  createConversation(callback: () => void) {
    this.srvConv.initChat(this.conv).then(() => {
      this.fetchConv(() => { callback(); });
    })
  }




}
