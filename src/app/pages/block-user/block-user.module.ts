import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BlockUserPageRoutingModule } from './block-user-routing.module';

import { BlockUserPage } from './block-user.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BlockUserPageRoutingModule,
    TranslateModule
  ],
  declarations: [BlockUserPage]
})
export class BlockUserPageModule { }
