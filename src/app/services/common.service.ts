import { Injectable, Output, EventEmitter } from '@angular/core';
import { ToastService } from './toast.service';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  @Output() refreshUserAppComponent: EventEmitter<any> = new EventEmitter(true);
  constructor(
    private toastService: ToastService,
    private plgDiagnostic: Diagnostic
  ) { }


  asyncLoop(iterations: number, func: Function, callback?: Function) {
    var index = 0;
    var done = false;
    var loop = {
      next: function () {
        if (done) {
          return;
        }
        if (index < iterations) {
          index++;
          func(loop);
        } else {
          done = true;
          if (callback) { callback("finish"); }
        }
      },
      iteration: function () {
        return index - 1;
      },
      break: function () {
        done = true;
        if (callback) { callback("break"); }
      }
    };
    loop.next();
    return loop;
  }

  ValidateEmail(email: string) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  getTimeStamp() {
    let date = new Date();
    return (date.getTime());
  }

  showMessage(options: IToastOption) {
    this.toastService.show(options);
  }

  APIErrorMessage() {
    this.showMessage({ message: "Error, Please try again later" });
  }

  isDisposableEmail(email: string) {
    let spamEmails: string[] = [
      "guerrillamail",
      "getairmail",
      "dispostabl",
      "fakeinbox",
      "10minutemail",
      "jetable",
      "burnthespam",
      "yopmail",
      "spamgourmet",
      "deadaddress",
      "e4ward",
      "eyepaste",
      "fakemailgenerator",
      "shitmail",
      "mailcatch",
      "mailexpire",
      "mailimate",
      "nospammers",
    ];
    email = email.toLowerCase();
    var hostName = email.substring(email.lastIndexOf("@") + 1);
    hostName = hostName.split(".")[0];
    return spamEmails.includes(hostName) ? true : false;
  }


  hasGpsLocation() {
    this.plgDiagnostic.isGpsLocationEnabled().then((enabled) => {
      console.log("GPS location is " + (enabled ? "enabled" : "disabled"));
      return enabled;
    }, (error) => {
      return false;
    });
  }

  distance(lat1, lon1, lat2, lon2, unit) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
      return 0;
    }
    else {
      var radlat1 = Math.PI * lat1 / 180;
      var radlat2 = Math.PI * lat2 / 180;
      var theta = lon1 - lon2;
      var radtheta = Math.PI * theta / 180;
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist);
      dist = dist * 180 / Math.PI;
      dist = dist * 60 * 1.1515;
      if (unit == "K") { dist = dist * 1.609344 }
      if (unit == "N") { dist = dist * 0.8684 }
      return dist;
    }
  }


  errorLog(message: string) {
    console.log("erro logs: ", message);
    // if (!window.isMobileWeb) {
    //   Raven.captureException(message)
    // };
  }


}
