import { Component, OnInit, Input } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import * as moment from 'moment';
import { SharedServiceService } from 'src/app/services/shared-service.service';
import { ChatViewPage } from '../chat-view/chat-view.page';
import { CoversationService } from 'src/app/services/coversation.service';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-user-detail-during-order',
  templateUrl: './user-detail-during-order.page.html',
  styleUrls: ['./user-detail-during-order.page.scss'],
})
export class UserDetailDuringOrderPage implements OnInit {
  userId: string;
  type: string;
  user: IUser;
  orderId: string;

  isChatExist: boolean = false;
  myChatUser: IChatUser = { unread_count: 0 };
  constructor(
    private navParams: NavParams,
    private srvFirebase: FirebaseService,
    private modalCtrl: ModalController,
    private srvShared: SharedServiceService,
    private srvConv: CoversationService,
    private callNumber: CallNumber
  ) {
    this.userId = this.navParams.get("userId");
    this.type = this.navParams.get("type");
    this.orderId = this.navParams.get("orderId");
    console.log()
    this.getUser();
    console.log("what is userId", this.userId);
    // if (this.type == 'drivers') {
    this.srvFirebase.isOrderChatExist(this.orderId, (resp) => {
      console.log(resp.message);
      switch (resp.status) {
        case "OK":
          this.isChatExist = true;
          this.myChatUser = this.srvConv.getMyUser(resp.message);
          console.log(this.myChatUser);
          break;

        default:
          break;
      }
    })
    // }
  }


  getUser() {
    this.srvFirebase.getUserByPhoneNumber(this.userId, this.type, (resp: ILocalResp) => {
      switch (resp.status) {
        case "OK":
          console.log(resp.message);
          this.user = resp.message;
          this.user.updatedDate = moment(this.user.updatedDate).format('lll');
          break;

        default:
          break;
      }
    });
  }

  ngOnInit() {
  }

  close() {
    this.modalCtrl.dismiss();
  }


  async openChat() {
    this.myChatUser.unread_count = 0;
    let conv = this.srvShared.createNewChat({
      device_token: '',
      isCreator: false,
      thumb: this.user && this.user['profilePic'] ? this.user['profilePic'] : null,
      unread_count: 0,
      userId: this.user.phoneNumber,
      username: this.user.displayName
    });

    conv.chatId = this.orderId;

    let modal = await this.modalCtrl.create({
      component: ChatViewPage,
      componentProps: {
        conv: conv
      }
    });
    modal.present();

  }


  callANumber() {
    this.callNumber.callNumber(this.user.phoneNumber, true).then(() => {

    }).catch(() => { });
  }

}
