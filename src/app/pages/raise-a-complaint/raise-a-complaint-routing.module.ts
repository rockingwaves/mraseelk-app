import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RaiseAComplaintPage } from './raise-a-complaint.page';

const routes: Routes = [
  {
    path: '',
    component: RaiseAComplaintPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RaiseAComplaintPageRoutingModule {}
