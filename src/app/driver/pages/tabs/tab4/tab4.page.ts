import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { NavController, ModalController } from '@ionic/angular';
import { FirebaseService } from 'src/app/services/firebase.service';
import { StorageService } from 'src/app/services/storage.service';
import { LoadingService } from 'src/app/services/loading.service';
import { AlertService } from 'src/app/services/alert.service';
import { TranslateService } from '@ngx-translate/core';

import * as _ from 'lodash';
import { RaiseAComplaintPage } from 'src/app/pages/raise-a-complaint/raise-a-complaint.page';
import { ChatViewPage } from 'src/app/modals/chat-view/chat-view.page';
import { SharedServiceService } from 'src/app/services/shared-service.service';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {
  user: IUser = this.srvUser.get();;
  isDriver: boolean = this.user.userType == 'driver';
  totalAmount: number = 0;
  constructor(
    private srvUser: UserService,
    private navCtrl: NavController,
    private srvFirebase: FirebaseService,
    private srvStorage: StorageService,
    private srvLoader: LoadingService,
    private srvAlert: AlertService,
    private translate: TranslateService,
    private modalCtrl: ModalController,
    private srvShared: SharedServiceService,
    private cdh: ChangeDetectorRef
  ) {
    this.getMyHistoryWork();
  }

  ngOnInit() {

  }


  editProfile() {
    this.navCtrl.navigateForward('edit-profile');
  }

  signOut() {
    this.srvAlert.confirm({ message: this.translate.instant("SIGNOUT_TEXT") }).then(() => {
      // this.srvLoader.showLoading();
      this.srvFirebase.signOut().then(() => {
        this.srvStorage.claerAll();
        this.srvLoader.hideLoading();
        this.cdh.detectChanges();
        this.navCtrl.navigateRoot('phone-auth');
      });
    }).catch(() => { });
  }

  paymentMethods() {
    this.navCtrl.navigateForward('credit-cards');
  }


  getMyHistoryWork() {
    this.srvFirebase.getMyHistoryWork((resp) => {
      switch (resp.status) {
        case "OK":
          this.totalAmount = _.sumBy(resp.message, (tO: IOrder) => { return tO.totalFares });
          console.log(this.totalAmount)
          break;

        default:
          break;
      }
    });
  }

  async raiseAComplaint() {
    let modal = await this.modalCtrl.create(
      {
        component: RaiseAComplaintPage,
      }
    );
    modal.present();
  }

  async openChat() {
    let conv = this.srvShared.createNewChat({
      device_token: '',
      isCreator: false,
      thumb: 'https://firebasestorage.googleapis.com/v0/b/mraseelk-5f2ec.appspot.com/o/login-logo.png?alt=media&token=e805ddaf-ad70-415d-9e73-2df31f9b45d2',
      unread_count: 0,
      userId: 'v7ahLXqhTEQWSTsStvRcY4CeRRp2',
      username: 'Mraseelk Admin'
    });


    let modal = await this.modalCtrl.create({
      component: ChatViewPage,
      componentProps: {
        conv: conv
      }
    });
    modal.present();

  }

}
