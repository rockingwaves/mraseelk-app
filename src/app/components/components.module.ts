import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { RestaurantItemListComponent } from './restaurant-item-list/restaurant-item-list.component';
import { OrderListComponent } from './order-list/order-list.component'
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { RateACustomerComponent } from './rate-a-customer/rate-a-customer.component';

@NgModule({
    imports: [IonicModule, CommonModule, TranslateModule],
    declarations: [RestaurantItemListComponent, OrderListComponent,RateACustomerComponent],
    exports: [RestaurantItemListComponent, OrderListComponent,RateACustomerComponent]
})
export class ComponentsModule { }

