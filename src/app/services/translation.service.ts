import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  constructor(
    private translate: TranslateService
  ) { }

  transform(value: string, args?: Object) {
    return new Promise((resolve) => {
      let translationVariable: Object = args ? args : null;
      this.translate.get(value, translationVariable).subscribe((res) => {
        resolve(res);
      });
    })
  }

}
