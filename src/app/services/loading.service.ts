import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { constant } from 'lodash';
import { Constant } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private loading: any;
  constructor(private loadingController: LoadingController) { }

  async showLoading(msg?: string) {
    let _msg = 'Please wait...'
    if (msg) {
      _msg = msg
    }
    this.loading = await this.loadingController.create({
      message: _msg,
      // duration: 2000
    });
    console.log("this.loading 1", Constant.loading);
    await this.loading.present();

    setTimeout(() => {
      this.loading.dismiss();
    }, 2000);

    // const { role, data } = await loading.onDidDismiss();
    // console.log('Loading dismissed!');
  }

  async hideLoading() {
    console.log("this.loading", Constant.loading);
    if (this.loading) {
      await this.loading.dismiss();
    }
  }



}
