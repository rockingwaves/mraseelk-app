import { Component } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { UserService } from 'src/app/services/user.service';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  arr: number[] = [1, 2, 3, 4, 5, 6];
  listCats: ICategory[] = [];
  products: IProducts[] = [];
  copyProducts: IProducts[] = [];
  shoppingCart: IShoppingCart = {
    products: []
  };
  constructor(
    private srvFirebase: FirebaseService,
    private userService: UserService,
    private navCtrl: NavController
  ) {
    this.getCats();
    this.getProducts();
  }

  ionViewWillEnter() {
    this.shoppingCart = this.userService.get().shoppingCart || {
      products: []
    };
  }

  getCats() {
    this.srvFirebase.getCategories().then((cats: ICategory[]) => {
      this.listCats = cats;
      this.listCats.forEach((cat) => { cat.isSelected = false; });
    }).catch((err) => {
      console.log(err);
    });
  }

  getProducts() {
    this.srvFirebase.getProducts().then((prods: IProducts[]) => {
      this.products = prods;
      this.copyProducts = this.products;
      console.log(this.products);
    }).catch((err) => {
      console.log(err);
    });
  }

  goToCart() {
    this.navCtrl.navigateForward('shopping-cart');
  }

  filterBy(cat: ICategory) {
    cat.isSelected = !cat.isSelected;
    this.products = this.copyProducts;
    this.products = this.products.filter((prods) => { return prods.categoryId == cat.id });
    this.listCats.forEach((_cat) => { _cat.id != cat.id ? _cat.isSelected = false : null; });
    if (!cat.isSelected) { this.products = this.copyProducts; }
  }



}
