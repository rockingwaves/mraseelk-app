import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  static iquestionnaire: User;
  constructor(
    private storageService: StorageService,
  ) { }

  init() {
    return new Promise((resolve, reject) => {
      this.storageService.getItem("user").then((info) => {
        UserService.iquestionnaire = new User(info);
        resolve(UserService.iquestionnaire);
      }, (error) => {
        UserService.iquestionnaire = new User(this.createEmptyUser());
        resolve(UserService.iquestionnaire);
      });
    });
  }

  createEmptyUser() {
    let user: IUser = {
      registeredDate: null,
      deviceToken: '',
      displayName: '',
      email: '',
      isLoggedIn: null,
      latitude: null,
      location: null,
      longitude: null,
      phoneNumber: '',
      photoUrl: null,
      platform: '',
      updatedDate: '',
      password: '',
      isAgreementSigned: false,
      permissions: {
        hasNotificationPermission: false,
        haslocationPermission: false
      },
      gender: '',
      rating: "0",
    }
    return user;
  }

  set(fields: IUser) {
    return new Promise((resolve, reject) => {
      let values = UserService.iquestionnaire;
      for (let f in fields) {
        values[f] = fields[f];
      }
      UserService.iquestionnaire = values;
      this.storageService.setItem("user", values).then((resp) => {
        resolve("ok");
      }, () => {
        console.log("error in setting");
        reject();
      });
    });
  }

  get() {
    return UserService.iquestionnaire;
  }

}
