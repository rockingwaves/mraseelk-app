import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ModalController, IonSlides } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { LocationService } from 'src/app/services/location.service';
import { SharedServiceService } from 'src/app/services/shared-service.service';
import { UserService } from 'src/app/services/user.service';
import { LoadingService } from 'src/app/services/loading.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { AlertService } from 'src/app/services/alert.service';
import { Constant } from 'src/app/services/constants.service';
import { PushNotificationService } from 'src/app/services/push-notification';
import * as moment from 'moment';


@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.page.html',
  styleUrls: ['./create-order.page.scss'],
})
export class CreateOrderPage implements OnInit {
  @ViewChild(IonSlides, { static: true }) slides: IonSlides;


  typeOfService: ISelectType[] = [
    { label: this.translate.instant("PICKUP_FROM_LOCTION_DELIVER_TO_CUSTOMER_LOCATION"), isSelected: true },
    { label: this.translate.instant("SEND_ITEM_FROM_CUS_LOC_TO_SOMEONES_LOC"), isSelected: false },
  ];
  selectedService: ISelectType = this.typeOfService[0];

  typeOfCar: ISelectType[] = [
    { label: this.translate.instant("MOTORCYCLE"), isSelected: true },
    { label: this.translate.instant("CAR"), isSelected: false },
    { label: this.translate.instant("TRUCK"), isSelected: false },
  ]
  selectedCar: ISelectType = this.typeOfCar[0];

  step: number = 0;
  user: IUser = this.srvUser.get();
  hasLocation: boolean = this.user.permissions ? this.user.permissions.haslocationPermission : false;


  pickup_dropoff_locs: IPDLocs = {
    dropoffLocation: '',
    dropoffLatLng: null,
    pickupLatLng: null,
    pickupLocation: '',
    orderDetail: '',
  }

  isDropoffSelected = false;
  isPickuoSelected = false;


  orderDetail: IOrder = {
    addedAt: null,
    comment: '',
    driverOffer: null,
    pickupLoc: null,
    status: 'pending',
    typeofService: '',
    userId: this.srvUser.get().phoneNumber,
    userName: this.srvUser.get().displayName,
    orderId: null,
    totalFares: 0,
    orderPriceOffer: {
      descp: '',
      price: ''
    },
    orderPriceStatus: 'requested'
  };


  isEditing: boolean = false;
  address: IAddress;

  constructor(
    private modalController: ModalController,
    private translate: TranslateService,
    private srvLocation: LocationService,
    private srvShared: SharedServiceService,
    private srvUser: UserService,
    private srvLoader: LoadingService,
    private srvFirebase: FirebaseService,
    private srvAlert: AlertService,
    private srvPush: PushNotificationService,
    private cdh: ChangeDetectorRef
  ) {

  }

  ngOnInit() {
    console.log(this.user);
    this.slides.lockSwipes(true);
    if (this.hasLocation) {
      this.createAddress();
    }
  }

  createAddress(callback?: () => void) {
    this.srvLocation.getCurrentLocation().then((location) => {
      // console.log("what is my location", location);
      this.srvLocation.geocode(location.coords).then((value) => {
        if (value.status == "OK") {
          let address = value.data;
          this.address = address;

          console.log(this.typeOfService[0].isSelected)
          if (this.typeOfService[0].isSelected) {
            this.pickup_dropoff_locs.pickupLatLng = location.coords;
            this.pickup_dropoff_locs.pickupLocation = address.address1 + "," + address.address2 + "," + address.city, + ", " + address.state;
            callback && callback();
          } else {
            this.pickup_dropoff_locs.dropoffLatLng = location.coords;
            this.pickup_dropoff_locs.dropoffLocation = address.address1 + "," + address.address2 + "," + address.city, + ", " + address.state;
            callback && callback();
          }

        }
      });
    });
  }

  goBack() {
    if (this.step == 1) {
      this.goToSlide1();
    } else if (this.step == 2) {
      this.goToSlide2();
    }
    else if (this.step == 3) {
      this.goToSlide3();
    }
    else if (this.step == 4) {
      this.goToSlide4();
    }
    else if (this.step == 5) {
      this.goToSlide5();
    } else {
      this.modalController.dismiss({ status: "CANCELLED" });
    }
  }

  selectServiceType(service: ISelectType) {
    this.typeOfService.forEach((s) => { s.isSelected = false; })
    service.isSelected = true;
    this.selectedService = service;
    // if (this.hasLocation) 
    this.createAddress(() => {
      setTimeout(() => {
        console.log("i am here in callback of createAddress");
        this.goToSlide2();
      }, 100);
    });
  }

  selectCarType(cartype: ISelectType) {
    this.typeOfCar.forEach((s) => { s.isSelected = false; })
    cartype.isSelected = true;
    this.selectedCar = cartype;
  }

  slideChanged() {
    this.slides.getActiveIndex().then((index) => {
      console.log(index);
      this.step = index;
    });
  }

  continue() {
    console.log(this.step);
    if (this.step == 0) {
      this.goToSlide2();
    } else if (this.step == 1) {
      this.calculateCahrges();
      this.goToSlide3();
    }
    else if (this.step == 2) {
      // this.goToSlide4();

      if (this.isEditing) {

      } else {
        // this.addOrder();
        this.sendPush();
      }
    }
  }

  addOrder() {
    this.orderDetail.typeofService = this.selectedService.label;

    this.orderDetail.pickupLoc = {
      address: this.pickup_dropoff_locs.pickupLocation,
      latlng: { latitude: this.pickup_dropoff_locs.pickupLatLng.latitude, longitude: this.pickup_dropoff_locs.pickupLatLng.longitude }
    };
    this.orderDetail.dropoffLoc = {
      address: this.pickup_dropoff_locs.dropoffLocation,
      latlng: { latitude: this.pickup_dropoff_locs.dropoffLatLng.latitude, longitude: this.pickup_dropoff_locs.dropoffLatLng.longitude }
    }
    this.orderDetail.comment = this.pickup_dropoff_locs.orderDetail;
    this.orderDetail.addedAt = moment().format('llll');
    this.orderDetail.userDeviceToken = this.user.deviceToken;
    // this.orderDetail.userId = this.user.phoneNumber;

    console.log(this.orderDetail);
    this.srvLoader.showLoading();
    this.srvFirebase.createOrder(this.orderDetail).then(() => {
      this.srvLoader.hideLoading();
      this.cdh.detectChanges();
      this.srvShared.$orderAdded.emit();
      this.modalController.dismiss({ status: "OK" });
    }).catch((err: string) => {
      this.srvLoader.hideLoading();
      this.cdh.detectChanges();
      this.srvAlert.show(err);
    });
  }



  goToSlide1() {
    this.slides.lockSwipes(false);
    this.slides.slideTo(0);
    this.slides.lockSwipes(true);
  }

  goToSlide2() {
    this.slides.lockSwipes(false);
    this.slides.slideTo(1);
    this.slides.lockSwipes(true);
  }

  goToSlide3() {
    this.slides.lockSwipes(false);
    this.slides.slideTo(2);
    this.slides.lockSwipes(true);
  }

  goToSlide4() {
    this.slides.lockSwipes(false);
    this.slides.slideTo(3);
    this.slides.lockSwipes(true);
  }

  goToSlide5() {
    this.slides.lockSwipes(false);
    this.slides.slideTo(4);
    this.slides.lockSwipes(true);
  }

  goToSlide6() {
    this.slides.lockSwipes(false);
    this.slides.slideTo(5);
    this.slides.lockSwipes(true);
  }



  enableLocation() {
    this.srvLocation.getCurrentLocation().then((location) => {
      this.user = this.srvUser.get();
      this.hasLocation = this.user.permissions.haslocationPermission;
      this.createAddress();
    }).catch(() => {

    });
  }

  selectLocation(type?: string) {
    if (type) this.setType(type);
    this.srvShared.selectLocation(this.pickup_dropoff_locs.pickupLatLng, (data: IAddressLatlng) => {
      this.address = data.address;

      if (this.isPickuoSelected) {
        this.pickup_dropoff_locs.pickupLatLng = data.latlng;
        this.pickup_dropoff_locs.pickupLocation = this.address.address1 + "," + this.address.address2 + "," + this.address.city, + ", " + this.address.state;;
      } else {
        this.pickup_dropoff_locs.dropoffLatLng = data.latlng;
        this.pickup_dropoff_locs.dropoffLocation = this.address.address1 + "," + this.address.address2 + "," + this.address.city, + ", " + this.address.state;;
      }
    });
  }

  setType(type) {
    this.isPickuoSelected = false;
    this.isDropoffSelected = false;
    if (type == 'PICK_UP') {
      this.isPickuoSelected = true;
    } else {
      this.isDropoffSelected = true;
    }
  }

  calculateCahrges() {
    console.log(this.pickup_dropoff_locs.pickupLatLng);
    console.log(this.pickup_dropoff_locs.dropoffLatLng);
    let distance = this.srvShared.getDistance({
      lat: this.pickup_dropoff_locs.pickupLatLng.latitude,
      lng: this.pickup_dropoff_locs.pickupLatLng.longitude
    }, {
      lat: this.pickup_dropoff_locs.dropoffLatLng.latitude,
      lng: this.pickup_dropoff_locs.dropoffLatLng.longitude,
    });
    console.log("distance ", distance);

    this.orderDetail.totalFares = distance < 5 ? Constant.appValues.drivertillKm : Math.round(Constant.appValues.driverPerkM * distance);
  }

  sendPush() {
    this.srvFirebase.getActiveDrivers((resp: ILocalResp) => {
      switch (resp.status) {
        case "OK":
          console.log("resp.message", resp.message);
          if (resp.message.length) {
            console.log("i am getting getDriversIn5Km")
            let drivers = this.getDriversIn5Km(resp.message);
            console.log("drivers", drivers);
            if (drivers.length) {
              this.sendRequest(drivers);
              this.addOrder();
            } else {
              this.srvAlert.show(this.translate.instant("NO_DRIVER_NEARBY_YOU"));
            }
          }
          break;

        default:
          break;
      }
    })

  }

  getDriversIn5Km(drivers: IUser[]) {
    // let filteredDrivers = drivers.filter((driver) => {
    //   if (driver.latitude && driver.longitude) {
    //     let distance = this.srvShared.getDistance(this.pickup_dropoff_locs.pickupLatLng, { latitude: driver.latitude, longitude: driver.longitude }) / 1000;
    //     console.log("distance drivers", distance);
    //     return distance <= 15;
    //   }
    // });
    console.log("filteredDrivers", drivers.length);
    return drivers;//filteredDrivers;
  }

  sendRequest(drivers: IUser[]) {
    console.log("drivers=>", drivers);
    drivers.forEach((d) => {
      this.srvPush.sendPush({
        token: d.deviceToken,
        bodyText: '',
        title: this.translate.instant("NEW_REQUEST"),
        type: 'new_order'
      });
    });
  }

}
// ""Pickup form my location and deliver to customer location",
// ""Pick item from customer location and deliver it to my location",