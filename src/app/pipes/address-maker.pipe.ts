import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'addressMaker'
})

export class AddressMakerPipe implements PipeTransform {

  transform(data: any, args?: any): any {
    let returnstring: string = '';
    if (data) {
      if (data.address1) {
        returnstring = data.address1 + " ";
      }
      if (data.address2) {
        returnstring = `${returnstring}${data.address2}, `;
      }
      if (data.city) {
        returnstring = `${returnstring}${data.city}, `;
      }
      if (data.state) {
        returnstring = `${returnstring}${data.state}`;
      }
      if (data.zipCode || data.zip) {
        returnstring = `${returnstring} ${(data.zipCode ? data.zipCode : data.zip)}`;
      }
    }
    return returnstring;
  }

}
