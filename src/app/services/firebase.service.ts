import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/analytics';
import 'firebase/storage'
import * as _ from 'lodash';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { CommonService } from './common.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class FirebaseService {

  constructor(
    private userService: UserService,
    private srvUser: UserService,
    private translate: TranslateService,
    private commonService: CommonService,
    private http: HttpClient
  ) { }

  initFirebase() {
    var firebaseConfig = {
      apiKey: "AIzaSyBxd_pRCTQfKgQG72Gfy5Aljr0SHNCWqro",
      authDomain: "mraseelk-5f2ec.firebaseapp.com",
      databaseURL: "https://mraseelk-5f2ec.firebaseio.com",
      projectId: "mraseelk-5f2ec",
      storageBucket: "mraseelk-5f2ec.appspot.com",
      messagingSenderId: "673968461626",
      appId: "1:673968461626:web:6381e89d2b0ef566342203",
      measurementId: "G-RS044821D8"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
  }

  checkIfAlreadyUserExists(phoneNumber: string) {
    return new Promise((resolve, reject) => {
      firebase.firestore().collection('users').doc(phoneNumber).get().then((doc) => {
        console.log("doc.exists: ", doc.exists);
        if (doc.exists) {
          resolve(doc.data());
        } else {
          reject();
        }
      });
    });
  }

  checkIfRegisteredAsDriver(phoneNumber: string) {
    return new Promise((resolve, reject) => {
      firebase.firestore().collection('drivers').doc(phoneNumber).get().then((doc) => {
        if (doc.exists) {
          resolve(doc.data());
        } else {
          reject();
        }
      });
    });
  }

  registerUser(user: IUser) {
    return new Promise((resolve) => {
      firebase.firestore().collection('users').doc(user.phoneNumber).set(Object.assign({}, user)).then((doc: any) => {
        var data: IUser = user;
        data.isLoggedIn = true;
        this.userService.set(data).then(() => {
          resolve(data);
        });
      });
    });
  }

  updatUserInfo(user: any, propertiesObject) {
    return new Promise((resolve, reject) => {
      firebase.firestore().collection('users').doc(user.phoneNumber).update(propertiesObject).then(() => {
        resolve();
      }, (err) => {
        console.log(`[Firebase==>updatUserInfo]: ${err}`);
      });
    });
  }

  updatDriverInfo(user: IUser, propertiesObject) {
    return new Promise((resolve, reject) => {
      firebase.firestore().collection('drivers').doc(user.phoneNumber).update(propertiesObject).then(() => {
        resolve();
      }, (err) => {
        console.log(`[Firebase==>updatUserInfo]: ${err}`);
      });
    });
  }


  loginUser(email, password) {
    return new Promise((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(email, password).then((res) => {
        this.checkIfAlreadyUserExists(res.user.uid).then((user: IUser) => {
          user.isLoggedIn = true;
          resolve(user);
        }).catch((err) => {
          reject(err.message);
        });
      }).catch((err) => {
        reject(err.message);
      })
    });
  }

  createUserAndReg(fbuser: IUser, callback: (resp: ILocalResp) => void) {
    console.log("fbuser", fbuser);
    let user: IUser = this.userService.get();
    user.email = fbuser.email;
    user.photoUrl = fbuser.photoUrl;
    user.phoneNumber = fbuser.phoneNumber;
    user.displayName = fbuser.displayName;
    user.registeredDate = moment().format('llll');
    user.updatedDate = moment().format('llll');
    user.platform = window.platform;
    console.log(user);
    this.registerUser(user).then(() => {
      user.isLoggedIn = true;
      this.srvUser.set(user).then(() => {
        callback({ status: "OK", message: null });
      })
    }).catch(err => {
      console.log(err);
      callback({ status: "ERROR", message: this.translate.instant("GENERAL_ERROR") });
    })
  }

  signOut() {
    return new Promise((resolve, reject) => {
      firebase.auth().signOut().then(function () {
        resolve();
      }).catch(function (error) {
        reject();
      });
    })
  }

  updateUserPassword(oldpassword, newPassword) {
    return new Promise((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(this.srvUser.get().email, oldpassword).then(() => {
        var user = firebase.auth().currentUser;
        console.log("user", user);
        user.updatePassword(newPassword).then(() => {
          resolve();
        }, err => {
          reject(err.message);
        });
      }).catch((err) => {
        reject(err.message);
      })
    });
  }

  uploadImageToFirebase(base64: string, progressCallback?: any) {
    return new Promise((resolve, reject) => {
      console.log('i am here');
      const filename = Math.floor(Date.now() / 1000);
      var storageRef: any = firebase.storage().ref().child(`adImages/` + filename);
      console.log('i am here2, ', base64);
      const uploadTask = storageRef.putString(base64, 'data_url');
      console.log('i am here 3');
      uploadTask.on("state_changed", (snapshot) => {
        console.log('i am here 4');
        var progress = parseInt(((snapshot.bytesTransferred / snapshot.totalBytes) * 100).toString());
        console.log('i am here 5: ', progress);
        if (progressCallback) {
          progressCallback({
            bytesTransferred: snapshot.bytesTransferred,
            totalBytes: snapshot.totalBytes,
            progress
          })
        };
      }, (err) => {
        console.log('erron upload: ', err);
        reject(err.message);
      }, () => {
        // on complete
        uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
          resolve(downloadURL);
        }, err => { reject(err.message); });
      });
    });
  }

  //#region store categories and products

  getCategories() {
    return new Promise((resolve, reject) => {
      let cats: ICategory[] = [];
      firebase.firestore().collection('categories').get().then((collection) => {
        if (collection.size) {
          collection.forEach((doc) => {
            doc.exists ? cats.push(<ICategory>doc.data()) : null;
          });
          resolve(cats);
        } else {
          reject("GENERAL_ERROR");
        }
      }).catch((err) => {
        reject(err.message);
      })
    });
  }

  getProducts() {
    return new Promise((resolve, reject) => {
      let products: IProducts[] = [];
      firebase.firestore().collection('products').get().then((collection) => {
        if (collection.size) {
          collection.forEach((doc) => {
            doc.exists ? products.push(<IProducts>doc.data()) : null;
          });
          resolve(products);
        } else {
          reject("GENERAL_ERROR");
        }
      }).catch((err) => {
        reject(err.message);
      })
    });
  }

  //#endregion

  //#region handle order 

  createOrder(order: IOrder) {
    return new Promise((resolve, reject) => {
      order.orderId = `id-${this.commonService.getTimeStamp()}`;
      firebase.firestore().collection('orders').doc(order.orderId).set(order).then(() => {
        resolve();
      }).catch((err) => {
        reject(err.message);
      });
    });
  }

  getMyHistoryOrders(callback: (resp: ILocalResp) => void) {
    let orders: IOrder[] = [];
    firebase.firestore().collection('orders')
      .where('userId', '==', this.srvUser.get().phoneNumber)
      .where('status', '==', 'completed').get().then((collection) => {
        if (!collection.size) callback && callback({ status: 'NO_ITEM_FOUND', message: "NO_ITEM_FOUND" });
        else {
          collection.forEach((doc) => {
            doc.exists ? orders.push(<IOrder>doc.data()) : null;
          });
          callback && callback({ status: 'OK', message: orders });
        }
      }).catch((err) => {
        callback && callback({ status: 'ERROR', message: err.message });
      });
  }

  getMyRunningOrders(callback: (resp: ILocalResp) => void) {
    let orders: IOrder[] = [];
    firebase.firestore().collection('orders')
      .where('userId', '==', this.srvUser.get().phoneNumber)
      // .where('status', '==', ('pending' || 'accepted' || 'arrived' || 'started'))
      .onSnapshot((collection) => {
        var source = collection.metadata.hasPendingWrites ? "Local" : "Server";
        if (source == 'Local') return;
        if (!collection.size) callback && callback({ status: 'NO_ITEM_FOUND', message: "NO_ITEM_FOUND" });
        else {
          collection.forEach((doc) => {
            doc.exists ? orders.push(<IOrder>doc.data()) : null;
          });
          callback && callback({ status: 'OK', message: orders });
        }
      }, (err) => {
        callback && callback({ status: 'ERROR', message: err.message });
      });
  }

  //#endregion




  //#region  Driver side 
  getPendingOrders(callback: (resp: ILocalResp) => void) {
    // return new Promise((resolve, reject) => {
    let orders: IOrder[] = [];
    // where('status', '==', 'pending')
    firebase.firestore().collection('orders').onSnapshot((res) => {
      var source = res.metadata.hasPendingWrites ? "Local" : "Server";
      if (source == 'Local') return;
      res.forEach((item: any) => {

        orders.push(item.data());
      });
      callback({ status: 'OK', message: orders });
    }, (err) => {
      callback && callback({ status: 'ERROR', message: err.message });
    });
    // })
  }

  getMyHistoryWork(callback: (resp: ILocalResp) => void) {
    let orders: IOrder[] = [];
    firebase.firestore().collection('orders')
      .where('status', '==', 'completed')
      .where('driverOffer.phoneNumber', '==', this.userService.get().phoneNumber)
      .get().then((collection) => {
        collection.forEach((item: any) => {
          orders.push(item.data());
        });
        callback({ status: 'OK', message: orders });
      }).catch((err) => {
        callback && callback({ status: 'ERROR', message: err.message });
      });
  }


  driverRealtimeEvents(orderId, callback: (resp: ILocalResp) => void) {
    firebase.firestore().collection('orders').doc(orderId).onSnapshot((doc) => {
      callback && callback({ status: "OK", message: doc.data() });
    });
  }

  orderAction(orderId, updatedObject: any, callback: (resp: ILocalResp) => void) {
    console.log("updatedObject", updatedObject);
    firebase.firestore().collection('orders').doc(orderId).update(updatedObject).then(() => {
      callback && callback({ status: 'OK', message: null });
    }).catch((err) => {
      callback && callback({ status: 'ERROR', message: err.message });
    })
  }

  getActiveDrivers(callback: (resp: ILocalResp) => void) {
    let drivers: IUser[] = [];
    firebase.firestore().collection('drivers')
      .where('status', '==', 'active')
      .get().then((coll) => {
        coll.forEach((doc) => {
          drivers.push(doc.data());
        });
        callback && callback({ status: 'OK', message: drivers });
      }).catch((err) => {
        callback && callback({ status: 'ERROR', message: err.message });
      })
  }

  //#endregion  Driver side 

  //#region product orders

  updateProductOrder(orderId: string, prop: object) {
    return new Promise((resolve, reject) => {
      firebase.firestore().collection('product-order').doc(orderId).update(prop).then((resp) => {
        resolve();
      }, (err) => {
        console.log(`[Firebase==>updateOrder]: ${err}`);
        resolve();
      });
    })
  }

  addProductOrder(order: IShoppingCart, callback: (resp: ILocalResp) => void) {
    order.userId = this.srvUser.get().phoneNumber;
    order.orderId = `id-${this.commonService.getTimeStamp()}`;
    order.dateAdded = window.moment().format('llll');
    firebase.firestore().collection('product-order').doc(order.orderId).set(order).then(() => {
      this.sendNewOrderMail(order.orderId);
      callback && callback({ status: 'OK', message: null });
    }).catch((err) => {
      callback && callback({ status: 'ERROR', message: err.message });
    });
  }

  getProductOrders(callback: (resp: ILocalResp) => void) {
    let orders: IShoppingCart[] = [];
    firebase.firestore().collection('product-order')
      .where('userId', '==', this.srvUser.get().phoneNumber)
      .get().then((collection) => {
        if (!collection.size) callback && callback({ status: 'NO_ITEM_FOUND', message: "NO_ITEM_FOUND" });
        else {
          collection.forEach((doc) => {
            doc.exists ? orders.push(<IOrder>doc.data()) : null;
          });
          callback && callback({ status: 'OK', message: orders });
        }
      }).catch((err) => {
        callback && callback({ status: 'ERROR', message: err.message });
      });
  }


  //#endregion product orders 


  getAppValues(callback: (resp: ILocalResp) => void) {
    return new Promise((resolve, reject) => {
      let data: IAppValue = null;
      firebase.firestore().collection('appValues').doc('variables').get().then((resp) => {
        console.log("in resolve deliveryFee: ", resp.data());
        data = <any>resp.data();
        callback && callback({ status: 'OK', message: data });
      }, (err) => {
        callback && callback({ status: 'ERROR', message: err.message });
      })
    })
  }

  sendNewOrderMail(productId: string) {
    let option: any = {
      productId
    }
    this.http.get(`https://us-central1-mraseelk-5f2ec.cloudfunctions.net/sendMail?dest=${JSON.stringify(option)}`).subscribe((resp) => {
      console.log("resp sendNewOrderMail: ", resp);
    })
  }

  sendComplain(complain: IComplain, callback: (resp: ILocalResp) => void) {
    firebase.firestore().collection('complains').doc(complain.id).set(complain).then(() => {
      this.sendComplaintMail(complain);
      callback && callback({ status: 'OK', message: null });
    }).catch((err) => {
      callback && callback({ status: 'OK', message: err.message });
    })
  }

  sendComplaintMail(complain: IComplain) {
    this.http.get(`https://us-central1-mraseelk-5f2ec.cloudfunctions.net/sendNewComplaineMail?dest=${JSON.stringify(complain)}`).subscribe((resp) => {
      console.log("resp sendNewOrderMail: ", resp);
    })
  }


  getUserByPhoneNumber(userId: string, type: string, callback: (resp: ILocalResp) => void) {
    firebase.firestore().collection(type).doc(userId).get().then((doc) => {
      doc.exists ? callback && callback({ status: "OK", message: doc.data() }) : callback && callback({ status: "ERROR", message: "" });
    }).catch((err) => {
      callback && callback({ status: "ERROR", message: err.message });
    });
  }


  isOrderChatExist(chatId: string, callback: (resp: ILocalResp) => void) {
    firebase.firestore().collection('conversations')
      .where('chatId', '==', chatId).get().then((doc) => {
        if (doc.size) {
          doc.forEach((doc) => {
            callback && callback({ status: "OK", message: doc.data() });
          })
        } else {
          callback && callback({ status: "ERROR", message: '' });
        }
      }).catch(() => {
        callback && callback({ status: "ERROR", message: '' });
      });
  }


  deleteConversation(convId: string, callback: (resp: ILocalResp) => void) {
    firebase.firestore().collection("conversations").doc(convId).collection("messages").get().then((docs) => {
      docs.forEach((doc) => {
        doc.ref.delete();
      });
      firebase.firestore().collection("conversations").doc(convId).delete().then(() => {
        callback && callback({ status: "OK", message: null });
      }).catch((err) => {
        callback && callback({ status: "ERROR", message: err.message });
      });
    })
  }


  updateDriver(driverId: string, props: any) {
    return firebase.firestore().collection('drivers').doc(driverId).update(props);
  }

}



